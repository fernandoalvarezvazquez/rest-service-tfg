package es.udc.tfg.restservice.config;

import javax.annotation.PostConstruct;

import es.udc.tfg.restservice.model.domain.BonoClases;
import es.udc.tfg.restservice.model.domain.Club;
import es.udc.tfg.restservice.model.domain.Partido;
import es.udc.tfg.restservice.model.domain.Pista;
import es.udc.tfg.restservice.model.domain.TipoPista;
import es.udc.tfg.restservice.model.domain.Usuario;
import es.udc.tfg.restservice.model.exception.UserLoginExistsException;
import es.udc.tfg.restservice.model.repository.*;
import es.udc.tfg.restservice.model.service.UsuarioService;
import es.udc.tfg.restservice.model.service.dto.UserDTOPublic;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Configuration
public class DatabaseLoader {
  private final Logger logger = LoggerFactory.getLogger(DatabaseLoader.class);

  @Autowired
  private UsuarioService userService;
  
  @Autowired
  private ClubDao clubDAO;
  
  @Autowired
  private PistaDao pistaDao;
  
  @Autowired
  private UsuarioDao userDAO;
  
  @Autowired
  private PartidoDao partidoDao;
  
  @Autowired
  private BonoClasesDao bonoDao;

  @Autowired
  private DatabaseLoader databaseLoader;

  /*
   * Para hacer que la carga de datos sea transacional, hay que cargar el propio
   * objeto como un bean y lanzar el método una vez cargado, ya que en el
   * PostConstruct (ni similares) se tienen en cuenta las anotaciones de
   * transaciones.
   */
  @PostConstruct
  public void init() {
    try {
      databaseLoader.loadData();
    } catch (UserLoginExistsException e) {
      logger.error(e.getMessage(), e);
    }
  }

  @Transactional(readOnly = false, rollbackFor = Exception.class)
  public void loadData() throws UserLoginExistsException {
    userService.registerUser("pepe", "pepe", true, true);
    userService.registerUser("admin_C1", "admin", true, false);
    userService.registerUser("admin_C2", "admin", true, false);
    userService.registerUser("admin_C3", "admin", true, false);
    userService.registerUser("admin_C4", "admin", true, false);
    userService.registerUser("admin_C5", "admin", true, false);
    userService.registerUser("admin_C6", "admin", true, false);
    userService.registerUser("admin_C7", "admin", true, false);
    userService.registerUser("admin_C8", "admin", true, false);
    userService.registerUser("maria", "maria");
    userService.registerUser("laura", "laura");
    userService.registerUser("pedro", "pedro");
    userService.registerUser("juan", "juan");
    userService.registerUser("alex", "alex");
    userService.registerUser("UserNoDisp", "cont", true, false);
    
    Club club1 = new Club("Sporting Club Casino", "Descripcion Club", "Calle A", "648801420", "C1", userDAO.findByLogin("admin_C1"), "");
    Club club2 = new Club("Coruña Sport Center", "Descripcion Club", "Calle B", "981981981", "C2", userDAO.findByLogin("admin_C2"), "");
    Club club3 = new Club("A1 padel", "Descripcion Club", "Calle C", "123123123", "C3", userDAO.findByLogin("admin_C3"), "");
    Club club4 = new Club("PadelPrix Coruña", "Descripcion Club", "Calle C", "456456456", "C4", userDAO.findByLogin("admin_C4"), "");
    Club club5 = new Club("Padel 0,0", "Descripcion Club", "Calle C", "897987987", "C5", userDAO.findByLogin("admin_C5"), "");
    Club club6 = new Club("Padel&Beer", "Descripcion Club", "Calle C", "987654321", "C5", userDAO.findByLogin("admin_C6"), "");
    Club club7 = new Club("Padel San Mateo", "Descripcion Club", "Calle C", "123456789", "C5", userDAO.findByLogin("admin_C7"), "");
    Club club8 = new Club("Smash Padel", "Descripcion Club", "Calle C", "123454321", "C5", userDAO.findByLogin("admin_C8"), "");
    
    clubDAO.create(club1);
    clubDAO.create(club2);
    clubDAO.create(club3);
    clubDAO.create(club4);
    clubDAO.create(club5);
    clubDAO.create(club6);
    clubDAO.create(club7);
    clubDAO.create(club8);
    
    
    Pista p11 = new Pista(club1, "pista 1", TipoPista.PADEL_DOBLES, true, 30.00);
    Pista p12 = new Pista(club1, "pista 2", TipoPista.PADEL_DOBLES, true, 30.00);
    Pista p13 = new Pista(club1, "pista 3", TipoPista.PADEL_DOBLES, true, 30.00);
    Pista p14 = new Pista(club1, "pista 4", TipoPista.PADEL_DOBLES, true, 30.00);
    Pista p15 = new Pista(club1, "pista 5", TipoPista.PADEL_DOBLES, true, 30.00);
    Pista p16 = new Pista(club1, "pista 6", TipoPista.PADEL_DOBLES, true, 30.00);
    
    pistaDao.create(p11);
    pistaDao.create(p12);
    pistaDao.create(p13);
    pistaDao.create(p14);
    pistaDao.create(p15);
    pistaDao.create(p16);
    
    Pista p21 = new Pista(club2, "pista 1", TipoPista.PADEL_DOBLES, true, 32.00);
    Pista p22 = new Pista(club2, "pista 2", TipoPista.PADEL_DOBLES, true, 32.00);
    Pista p23 = new Pista(club2, "pista 3", TipoPista.PADEL_DOBLES, true, 32.00);
    Pista p24 = new Pista(club2, "pista 4", TipoPista.PADEL_DOBLES, true, 32.00);
    Pista p25 = new Pista(club2, "pista 5", TipoPista.PADEL_DOBLES, true, 32.00);
    Pista p26 = new Pista(club2, "pista 6", TipoPista.PADEL_DOBLES, true, 32.00);
    
    pistaDao.create(p21);
    pistaDao.create(p22);
    pistaDao.create(p23);
    pistaDao.create(p24);
    pistaDao.create(p25);
    pistaDao.create(p26);
    
    
    
    Partido partido1 = new Partido(userDAO.findByLogin("pedro"), LocalDateTime.now().withHour(10).withMinute(0).withSecond(0).withNano(0), 
    		LocalDateTime.now().withHour(10).withMinute(0).withSecond(0).withNano(0).plusMinutes(90), p11, false, 45, "", "");
    partido1.addJugador(userDAO.findByLogin("maria"));
    partido1.addJugador(userDAO.findByLogin("juan"));
    partidoDao.create(partido1);
    
    Partido partido2 = new Partido(userDAO.findByLogin("pedro"), LocalDateTime.now().plusDays(1).withHour(10).withMinute(0).withSecond(0).withNano(0), 
    		LocalDateTime.now().plusDays(1).withHour(10).withMinute(0).withSecond(0).withNano(0).plusMinutes(90), p22, false, 45, "", "");
    partidoDao.create(partido2);
    
    Partido partido3 = new Partido(userDAO.findByLogin("pedro"), LocalDateTime.now().minusDays(2).withHour(13).withMinute(0).withSecond(0).withNano(0), 
    		LocalDateTime.now().minusDays(2).withHour(13).withMinute(0).withSecond(0).withNano(0).plusMinutes(90), p11, false, 45, "", "");
    partidoDao.create(partido3);
    
    Partido partido4 = new Partido(userDAO.findByLogin("pedro"), LocalDateTime.now().minusDays(1).withHour(10).withMinute(0).withSecond(0).withNano(0), 
    		LocalDateTime.now().minusDays(1).withHour(10).withMinute(0).withSecond(0).withNano(0).plusMinutes(90), p22, false, 45, "", "");
    partido4.addJugador(userDAO.findByLogin("maria"));
    partido4.addJugador(userDAO.findByLogin("juan"));
    partido4.addJugador(userDAO.findByLogin("alex"));
    partidoDao.create(partido4);
    
    BonoClases bono1 = new BonoClases("Bono 10 Clases", "10 Clases de 1 hora con el profesor que quieras a la hora que quieras", 200.0, 10, club1, null);
    bonoDao.create(bono1);
    
    BonoClases bono2 = new BonoClases("Bono 20 Clases", "20 Clases de 1 hora con el profesor que quieras a la hora que quieras", 250.0, 20, club2, null);
    bonoDao.create(bono2);
  }
}
