package es.udc.tfg.restservice.model.service;


import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.udc.tfg.restservice.model.domain.BonoClases;
import es.udc.tfg.restservice.model.domain.Club;
import es.udc.tfg.restservice.model.domain.Usuario;
import es.udc.tfg.restservice.model.exception.NotFoundException;
import es.udc.tfg.restservice.model.repository.BonoClasesDao;
import es.udc.tfg.restservice.model.repository.ClubDao;
import es.udc.tfg.restservice.model.repository.UsuarioDao;
import es.udc.tfg.restservice.model.service.dto.BonoClasesDTO;

@Service
@Transactional(readOnly= true, rollbackFor = Exception.class)
public class BonoClasesService {

	@Autowired
	private BonoClasesDao bonoDao;
	
	@Autowired
	private ClubDao clubDao;
	
	@Autowired
	private UsuarioDao usuarioDao;
	
	public List<BonoClasesDTO> findAll(){
		return bonoDao.findAll().stream().map(bono -> new BonoClasesDTO(bono)).collect(Collectors.toList());
	}
	
	public List<BonoClasesDTO> findAllByClub(Long id) throws NotFoundException{
		Club club = clubDao.findById(id);
		if (club == null) {
			throw new NotFoundException(id.toString(),Club.class);
		}
		return bonoDao.findAllByClub(id).stream().map(bono -> new BonoClasesDTO(bono))
				.collect(Collectors.toList());
	}
	
	public BonoClasesDTO findById(Long id) throws NotFoundException{
		BonoClases bono = bonoDao.findById(id);
		if ( bono == null) {
			throw new NotFoundException(id.toString(),BonoClases.class);
		}
		return new BonoClasesDTO(bono);
	}
	
	public List<BonoClasesDTO> findByJugador(Long id) throws NotFoundException{
		Usuario jugador = usuarioDao.findById(id);
		if (jugador == null) {
			throw new NotFoundException(id.toString(), Usuario.class);
		}
		return bonoDao.findByJugador(id).stream().map(bono -> new BonoClasesDTO(bono))
				.collect(Collectors.toList());
	}
	
	@PreAuthorize("hasAuthority('ADMIN_CLUB')")
	@Transactional(readOnly = false)
	public BonoClasesDTO create(BonoClasesDTO bono) {
		
		BonoClases bdBono = new BonoClases(bono.getNombre(), bono.getDescripcion(), bono.getPrecio(), bono.getNum_clases(), clubDao.findById(bono.getIdClub()), null);
		bonoDao.create(bdBono);
		return new BonoClasesDTO(bdBono);
	}
	
	@PreAuthorize("isAuthenticated()")
	@Transactional(readOnly = false)
	public BonoClasesDTO update(BonoClasesDTO bono) {
		BonoClases bdBono = bonoDao.findById(bono.getId());
		bdBono.setNombre(bono.getNombre());
		bdBono.setDescripcion(bono.getDescripcion());
		bdBono.setPrecio(bono.getPrecio());
		bdBono.setNum_clases(bono.getNum_clases());
		if(bono.getIdUser() != null)
			bdBono.setJugador(usuarioDao.findById(bono.getIdUser()));
		bonoDao.update(bdBono);
		return new BonoClasesDTO(bdBono);
	}
	
	@PreAuthorize("isAuthenticated()")
	@Transactional(readOnly = false)
	public BonoClasesDTO updateComprador(BonoClasesDTO bono) {
		BonoClases bdBono = bonoDao.findById(bono.getId());
		if(bono.getIdUser() != null)
			bdBono.setJugador(usuarioDao.findById(bono.getIdUser()));
		bonoDao.update(bdBono);
		return new BonoClasesDTO(bdBono);
	}
	
	@PreAuthorize("hasAnyAuthority('ADMIN_WEB', 'ADMIN_CLUB')")
	@Transactional(readOnly = false)
	public void deleteById(Long id) throws NotFoundException{
		BonoClases bono = bonoDao.findById(id);
		
		if (bono == null) {
			throw new NotFoundException(id.toString(), BonoClases.class);
		}
		bonoDao.delete(bono);
	}
}









