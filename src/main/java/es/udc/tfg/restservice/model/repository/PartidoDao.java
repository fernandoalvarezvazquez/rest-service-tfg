package es.udc.tfg.restservice.model.repository;

import java.time.LocalDateTime;
import java.util.List;

import es.udc.tfg.restservice.model.domain.Partido;
import es.udc.tfg.restservice.model.domain.Usuario;

public interface PartidoDao {
	
	List<Partido> findAll();
	
	Partido findById(Long id);
	
	Partido findByPistaHora(Long idPista, LocalDateTime hora);
	
	List<Partido> findByClub(Long idClub);
	
	void deleteById(Long id);

	void create(Partido partido);

	void update(Partido partido);

	void delete(Partido partido);
}
