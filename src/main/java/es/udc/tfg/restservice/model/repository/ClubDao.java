package es.udc.tfg.restservice.model.repository;

import java.util.List;

import es.udc.tfg.restservice.model.domain.Club;
import es.udc.tfg.restservice.model.domain.Usuario;

public interface ClubDao {

	List<Club> findAll();
	
	Club findById(Long id);
	
	List<Club> findByName(String nombre);
	
	Club findByAdmin(Usuario user);
	
	void deleteById(Long id);

	void create(Club club);

	void update(Club club);

	void delete(Club club);
	
}
