package es.udc.tfg.restservice.model.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class BonoClases {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "bono_generator")
	@SequenceGenerator(name = "bono_generator", sequenceName = "bono_seq")
	@Column(name = "id_bono")
	private Long id;
	
	private String nombre;
	
	private String descripcion;
	
	private Double precio;
	
	private int num_clases;
	
	@ManyToOne
	@JoinColumn(name = "club_bono", nullable = false)
	private Club club;
	
	@ManyToOne
	@JoinColumn(name = "jug_bono", nullable = true)
	private Usuario jugador;
	
	public BonoClases() {}
	
	public BonoClases(String nombre, String descripcion, Double precio, int num_clases, Club club, Usuario jugador) {
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.precio = precio;
		this.num_clases = num_clases;
		this.club = club;
		this.jugador = jugador;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Double getPrecio() {
		return precio;
	}

	public void setPrecio(Double precio) {
		this.precio = precio;
	}

	public int getNum_clases() {
		return num_clases;
	}

	public void setNum_clases(int num_clases) {
		this.num_clases = num_clases;
	}

	public Club getClub() {
		return club;
	}

	public void setClub(Club club) {
		this.club = club;
	}

	public Usuario getJugador() {
		return jugador;
	}

	public void setJugador(Usuario jugador) {
		this.jugador = jugador;
	}
	
	

}
