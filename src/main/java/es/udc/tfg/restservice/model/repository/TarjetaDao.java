package es.udc.tfg.restservice.model.repository;

import java.util.List;

import es.udc.tfg.restservice.model.domain.Tarjeta;

public interface TarjetaDao {

	List<Tarjeta> findAll();
	
	Tarjeta findById(Long id);
	
	List<Tarjeta> findByPlayer(Long id);
	
	void deleteById(long id);
	
	void create(Tarjeta tarjeta);
	
	void update(Tarjeta tarjeta);
	
	void delete(Tarjeta tarjeta);
	
}
