package es.udc.tfg.restservice.model.service;

import es.udc.tfg.restservice.model.domain.*;
import es.udc.tfg.restservice.model.exception.NotFoundException;
import es.udc.tfg.restservice.model.exception.OperationNotAllowed;
import es.udc.tfg.restservice.model.exception.UserLoginExistsException;
import es.udc.tfg.restservice.model.repository.ClubDao;
import es.udc.tfg.restservice.model.repository.PartidoDao;
import es.udc.tfg.restservice.model.repository.UsuarioDao;
import es.udc.tfg.restservice.model.service.dto.PartidoDTO;
import es.udc.tfg.restservice.model.service.dto.TarjetaDTO;
import es.udc.tfg.restservice.model.service.dto.UserDTOPrivate;
import es.udc.tfg.restservice.model.service.dto.UserDTOPublic;
import es.udc.tfg.restservice.security.SecurityUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class UsuarioService {

  @Autowired
  private PasswordEncoder passwordEncoder;

  @Autowired
  private UsuarioDao userDAO;
  
  @Autowired
  private PartidoDao partidoDao;
  
  @Autowired
  private ClubDao clubDao;

  public List<UserDTOPublic> findAll() {
    return userDAO.findAll().stream().map(user -> new UserDTOPublic(user)).collect(Collectors.toList());
  }
  
  public List<UserDTOPublic> findAllJugadores(){
	  return userDAO.findAllJugadores().stream().map(user -> new UserDTOPublic(user)).collect(Collectors.toList());
  }

  public UserDTOPublic findById(Long id) throws NotFoundException {
    Usuario user = userDAO.findById(id);
    if (user == null) {
      throw new NotFoundException(id.toString(), Usuario.class);
    }
    return new UserDTOPublic(userDAO.findById(id));
  }

  public UserDTOPublic findByLogin(String login) throws NotFoundException {
    Usuario user = userDAO.findByLogin(login);
    if (user == null) {
      throw new NotFoundException(login.toString(), Usuario.class);
    }
    return new UserDTOPublic(userDAO.findByLogin(login));
  }

  public List<PartidoDTO> findPartidos(Long id) throws NotFoundException{
	  Usuario u = userDAO.findById(id);
	  if (u == null)
		  throw new NotFoundException(id.toString(), Usuario.class);
	  return userDAO.findPartidos(id).stream().map(p -> new PartidoDTO(p)).collect(Collectors.toList());
  }
  
  public List<TarjetaDTO> findTarjetas(Long id) throws NotFoundException{
	  Usuario u = userDAO.findById(id);
	  if (u == null)
		  throw new NotFoundException(id.toString(), Usuario.class);
	  return userDAO.findTarjetas(id).stream().map(t -> new TarjetaDTO(t)).collect(Collectors.toList());
  }
  
  public String saveImage(Long id, MultipartFile file) throws IOException, NotFoundException {
		Usuario user = userDAO.findById(id);
      
		if (user == null) {
      	throw new NotFoundException(id.toString(), Usuario.class);
      }
      String fileName = StringUtils.cleanPath(file.getOriginalFilename());
      Path path = Paths.get("uploads/" + fileName);
      Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
      
      user.setFotoPerfil(path.toString());
      userDAO.update(user);
      
      return path.toString();
  }
  
  @Transactional(readOnly = false)
  public void registerUser(String login, String contrasena) throws UserLoginExistsException {
    registerUser(login, contrasena, false, false);
  }

  @Transactional(readOnly = false)
  public void registerUser(String login, String contrasena, boolean isAdmin, boolean isAdminWeb) throws UserLoginExistsException {
    if (userDAO.findByLogin(login) != null) {
      throw new UserLoginExistsException(login);
    }

    Usuario user = new Usuario();
    String encryptedPassword = passwordEncoder.encode(contrasena);

    user.setLogin(login);
    user.setContrasena(encryptedPassword);
    if (isAdmin) {
    	if (isAdminWeb) {
	      user.setAutoridad(UserAuthority.ADMIN_WEB);
	    }
	    else {
	    	user.setAutoridad(UserAuthority.ADMIN_CLUB);
	    }
    } else {
    	user.setAutoridad(UserAuthority.PLAYER);
	}
    user.setNivel(2.0);
    user.setFotoPerfil("");
    userDAO.create(user);
  }

  @PreAuthorize("isAuthenticated()")
  @Transactional(readOnly = false)
  public UserDTOPublic update(UserDTOPublic user){
    Usuario bdUser = userDAO.findById(user.getId());
    bdUser.setLogin(user.getLogin());
    bdUser.setAutoridad(user.getAuthority());
    bdUser.setNivel(user.getNivel());
    bdUser.setFotoPerfil(user.getFotoPerfil());
    
    userDAO.update(bdUser);
    return new UserDTOPublic(bdUser);
  }
  
  @PreAuthorize("isAuthenticated()")
  @Transactional(readOnly = false)
  public void updateFoto(UserDTOPublic user){
    Usuario bdUser = userDAO.findById(user.getId());
    bdUser.setFotoPerfil(user.getFotoPerfil());
    
    userDAO.update(bdUser);
    //return new UserDTOPublic(bdUser);
  }
  
  @PreAuthorize("isAuthenticated()")
  @Transactional(readOnly = false)
  public UserDTOPublic updateNivel(UserDTOPublic user){
    Usuario bdUser = userDAO.findById(user.getId());
    bdUser.setNivel(user.getNivel());
    
    userDAO.update(bdUser);
    return new UserDTOPublic(bdUser);
  }
  
  

  @PreAuthorize("isAuthenticated()")
  @Transactional(readOnly = false)
  public UserDTOPublic updateDatos(Long id) throws NotFoundException{
    Usuario user = userDAO.findById(id);
    if (user == null) {
      throw new NotFoundException(id.toString(), Usuario.class);
    }

    /*List<Producto> prods = userDAO.findAllVentas(id);
    int media = 0;
    int sum = 0;
    int cont = 1;
    for(int i = 0; i<prods.size(); i++){
      if(prods.get(i).getValoracion() != 0){
        cont++;
        sum = sum + prods.get(i).getValoracion();
      }
    }
    media = sum/cont;
    user.setValoracionMedia(media);
    userDAO.update(user);*/
    return new UserDTOPublic(user);
  }

  @PreAuthorize("isAuthenticated()")
  @Transactional(readOnly = false)
  public UserDTOPrivate updateContrasena(String name, String contrasena) throws NotFoundException{
    Usuario user = userDAO.findByLogin(name);
    if (user == null) {
      throw new NotFoundException(name, Usuario.class);
    }

    user.setContrasena(contrasena);
    userDAO.update(user);
    return new UserDTOPrivate(user);
  }

  public UserDTOPrivate getCurrentUserWithAuthority() {
    String currentUserLogin = SecurityUtils.getCurrentUserLogin();
    if (currentUserLogin != null) {
      return new UserDTOPrivate(userDAO.findByLogin(currentUserLogin));
    }
    return null;
  }

  @PreAuthorize("hasAuthority('ADMIN_WEB')")
  @Transactional(readOnly = false)
  public void deleteById(Long id) throws NotFoundException, OperationNotAllowed {
    Usuario user = userDAO.findById(id);
    Usuario noUser = userDAO.findByLogin("UserNoDisp");
    if (user == null) {
      throw new NotFoundException(id.toString(), Usuario.class);
    }
    	
    if(user.getAutoridad() == UserAuthority.ADMIN_CLUB && user.getMiClub() != null) {
    	Club club = clubDao.findByAdmin(user);
    	List<Partido> partidosClub = partidoDao.findByClub(club.getId());
    	for (Partido partido : partidosClub) {
			if (partido.getCreador() == user)
				partido.setCreador(noUser);
			partidoDao.update(partido);
		}
    	club.setAdminClub(null);
    	clubDao.update(club);
    }
    
    if (user.getAutoridad() == UserAuthority.PLAYER) {
    	List<Partido> partidos = userDAO.findPartidos(user.getId());
	    for (Partido partido : partidos) {
	    	if(partido.getCreador() == user)
	    		partido.setCreador(noUser);
			partido.deleteJugador(user);
			partidoDao.update(partido);
		}
    }
    	
    
    /*List<Producto> prods = prodDao.findAll();

    for (int i = 0; i<prods.size(); i++){
      if (prods.get(i).getVendedor() == user && prods.get(i).getComprador() == null){
        prodDao.delete(prods.get(i));
      }
      else if (prods.get(i).getVendedor() == user && prods.get(i).getComprador().getId() != null){
        prods.get(i).setVendedor(null);
        prodDao.update(prods.get(i));
      }
      else if (prods.get(i).getComprador() != null && prods.get(i).getComprador() == user){
        prods.get(i).setComprador(null);
        prodDao.update(prods.get(i));
      }
      else{
        List<Usuario> mg = prods.get(i).getMeGustaList();
        for(int x = mg.size()-1; x>=0; x--){
          if(user.getLogin() == mg.get(x).getLogin()){
            mg.remove(x);
          }
        }
        prodDao.update(prods.get(i));
      }
    }

    List<Mensaje> mens = userDAO.findAllMensajeById(id);
    for (int i = 0; i<mens.size(); i++){
      mensajeDAO.delete(mens.get(i));
    }*/
    userDAO.delete(user);
  }
}
