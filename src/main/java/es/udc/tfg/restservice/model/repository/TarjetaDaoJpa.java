package es.udc.tfg.restservice.model.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import es.udc.tfg.restservice.model.domain.Tarjeta;
import es.udc.tfg.restservice.model.repository.util.GenericDaoJpa;

@Repository
public class TarjetaDaoJpa extends GenericDaoJpa implements TarjetaDao{

	@Override
	public List<Tarjeta> findAll() {
		return entityManager.createQuery("from Tarjeta", Tarjeta.class).getResultList();
	}

	@Override
	public Tarjeta findById(Long id) {
		return entityManager.find(Tarjeta.class, id);
	}
	
	@Override
	public List<Tarjeta> findByPlayer(Long id){
		return entityManager.createQuery("from Tarjeta t where t.jugador.id = :id", Tarjeta.class).setParameter("id", id).getResultList();
	}

	@Override
	public void deleteById(long id) {
		Tarjeta t = findById(id);
		delete(t);
	}

	@Override
	public void create(Tarjeta tarjeta) {
		entityManager.persist(tarjeta);
	}

	@Override
	public void update(Tarjeta tarjeta) {
		entityManager.merge(tarjeta);
	}

	@Override
	public void delete(Tarjeta tarjeta) {
		entityManager.remove(tarjeta);
	}

}
