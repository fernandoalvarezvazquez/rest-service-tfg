package es.udc.tfg.restservice.model.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class Club {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "club_generator")
	@SequenceGenerator(name = "club_generator", sequenceName = "club_seq")
	@Column(name = "id_club")
	private Long id;
	
	@Column(unique = true)
	private String nombre;
	
	@Column(nullable = false)
	private String descripcion;
	
	@Column(nullable = false)
	private String direccion;
	
	@Column(nullable = false)
	private String telefono;
	
	@Column(nullable = false)
	private String siglas;
	
	private String imagenPath;
	
	@Column(nullable = false)
	private int num_pistas;
	
	@OneToMany(mappedBy = "club")
	private List<Pista> pistas = new ArrayList<Pista>();
	
	/*@ManyToMany(mappedBy = "misClubes")
	private List<Usuario> jugadores = new ArrayList<Usuario>();*/
	
	@OneToOne
	@JoinColumn(name = "adminClub", nullable = true)
	private Usuario adminClub;
	
	/*@ManyToOne
	@JoinColumn(name = "clubs")
	private Usuario adminWeb;*/
	
	@OneToMany(mappedBy = "club")
	private List<BonoClases> bonos;
	
	@ManyToMany(mappedBy = "clubesFav")
	private List<Usuario> jugadoresFav;
	
	public Club() {}
	
	public Club(String nombre, String descripcion, String direccion, 
			String telefono, String siglas, Usuario adminClub, String imagen){
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.direccion = direccion;
		this.telefono = telefono;
		this.siglas = siglas;
		this.num_pistas = pistas.size();
		this.adminClub = adminClub;
		this.imagenPath = imagen;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	
	public String getSiglas() {
		return siglas;
	}

	public void setSiglas(String siglas) {
		this.siglas = siglas;
	}

	public String getImagenPath() {
		return imagenPath;
	}

	public void setImagenPath(String imagenPath) {
		this.imagenPath = imagenPath;
	}

	public int getNum_pistas() {
		return num_pistas;
	}

	public Usuario getAdminClub() {
		return adminClub;
	}

	public void setAdminClub(Usuario adminClub) {
		this.adminClub = adminClub;
	}

	public void addPista(Pista pista) {
		this.pistas.add(pista);
	}
	
	public List<Pista> getPistas() {
		return pistas;
	}

	public void setPistas(List<Pista> pistas) {
		this.pistas = pistas;
	}

	public List<BonoClases> getBonos() {
		return bonos;
	}

	public void setBonos(List<BonoClases> bonos) {
		this.bonos = bonos;
	}

	public List<Usuario> getJugadoresFav() {
		return jugadoresFav;
	}

	public void setJugadoresFav(List<Usuario> jugadoresFav) {
		this.jugadoresFav = jugadoresFav;
	}
	
}
