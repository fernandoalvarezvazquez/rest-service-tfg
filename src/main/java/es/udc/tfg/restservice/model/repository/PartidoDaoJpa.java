package es.udc.tfg.restservice.model.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.stereotype.Repository;

import es.udc.tfg.restservice.model.domain.Partido;
import es.udc.tfg.restservice.model.domain.Usuario;
import es.udc.tfg.restservice.model.repository.util.GenericDaoJpa;

@Repository
public class PartidoDaoJpa  extends GenericDaoJpa implements PartidoDao{

	@Override
	public List<Partido> findAll() {
		return entityManager.createQuery("from Partido p", Partido.class).getResultList();
	}

	@Override
	public Partido findById(Long id) {
		return entityManager.find(Partido.class, id);
	}
	
	@Override
	public Partido findByPistaHora(Long idPista, LocalDateTime hora) {
		return entityManager.createQuery("from Partido p where p.pista.id = :idPista AND p.hora_ini = :hora", Partido.class)
				.setParameter("idPista", idPista).setParameter("hora", hora).getSingleResult();
	}
	
	@Override
	public List<Partido> findByClub(Long idClub) {
		return entityManager.createQuery("FROM Partido p JOIN FETCH p.pista WHERE p.pista.club.id = :idClub order by p.hora_ini", Partido.class)
				.setParameter("idClub", idClub).getResultList();
		
	}
	
	@Override
	public void deleteById(Long id) {
		Partido partido = findById(id);
		delete(partido);
	}

	@Override
	public void create(Partido partido) {
		entityManager.persist(partido);
	}

	@Override
	public void update(Partido partido) {
		entityManager.merge(partido);
	}

	@Override
	public void delete(Partido partido) {
		entityManager.remove(partido);
	}

	
}
