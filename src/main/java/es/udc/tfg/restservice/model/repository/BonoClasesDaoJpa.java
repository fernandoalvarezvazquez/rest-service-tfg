package es.udc.tfg.restservice.model.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import es.udc.tfg.restservice.model.domain.BonoClases;
import es.udc.tfg.restservice.model.repository.util.GenericDaoJpa;

@Repository
public class BonoClasesDaoJpa extends GenericDaoJpa implements BonoClasesDao{

	@Override
	public List<BonoClases> findAll(){
		return entityManager.createQuery("from BonoClases", BonoClases.class).getResultList();
	}
	
	@Override
	public List<BonoClases> findAllByClub(Long idClub) {
		return entityManager.createQuery("from BonoClases b where b.club.id = :idClub", BonoClases.class)
				.setParameter("idClub", idClub).getResultList();
	}
	
	@Override
	public BonoClases findById(Long id) {
		return entityManager.find(BonoClases.class, id);
	}

	@Override
	public List<BonoClases> findByJugador(Long idUser){
		return entityManager.createQuery("from BonoClases b where b.jugador.id = :idUser", BonoClases.class)
				.setParameter("idUser", idUser).getResultList();
	}
	
	@Override
	public void deleteById(Long id) {
		BonoClases bono = findById(id);
		delete(bono);
	}

	@Override
	public void create(BonoClases bono) {
		entityManager.persist(bono);
	}

	@Override
	public void update(BonoClases bono) {
		entityManager.merge(bono);
	}

	@Override
	public void delete(BonoClases bono) {
		entityManager.remove(bono);
	}
}
