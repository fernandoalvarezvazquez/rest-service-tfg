package es.udc.tfg.restservice.model.repository;

import java.util.List;

import es.udc.tfg.restservice.model.domain.Transaccion;

public interface TransaccionDao {

	List<Transaccion> findAll();
	
	Transaccion findById(Long id);
	
	void deleteById(Long id);
	
	void create(Transaccion transaccion);
	
	void update(Transaccion transaccion);
	
	void delete(Transaccion transaccion);
}
