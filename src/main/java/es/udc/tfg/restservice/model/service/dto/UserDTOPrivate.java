package es.udc.tfg.restservice.model.service.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import es.udc.tfg.restservice.model.domain.Usuario;

public class UserDTOPrivate {
	private Long id;

	@NotEmpty
	@Size(min = 4)
	private String login;

	@NotEmpty
	@Size(min = 4)
	private String contrasena;

	private String autoridad;

	private double nivel;
	private String fotoPerfil;

	public UserDTOPrivate() {
	}

	public UserDTOPrivate(Usuario user) {
		this.id = user.getId();
		this.login = user.getLogin();
		// la contraseña no se rellena, nunca se envía al cliente
		this.autoridad = user.getAutoridad().name();
		if (user.getNivel() != 0.0)
	    	this.nivel = user.getNivel();
	    else this.nivel = 0.0;
		this.fotoPerfil = user.getFotoPerfil();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getContrasena() {
		return contrasena;
	}

	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}

	public String getAuthority() {
		return autoridad;
	}

	public void setAuthority(String authority) {
		this.autoridad = authority;
	}

	public double getNivel() {
		return nivel;
	}

	public void setNivel(double nivel) {
		this.nivel = nivel;
	}

	public String getFotoPerfil() {
		return fotoPerfil;
	}

	public void setFotoPerfil(String fotoPerfil) {
		this.fotoPerfil = fotoPerfil;
	}

}
