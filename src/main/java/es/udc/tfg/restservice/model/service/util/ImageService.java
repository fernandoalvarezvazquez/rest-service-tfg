package es.udc.tfg.restservice.model.service.util;

import org.springframework.web.multipart.MultipartFile;

import es.udc.tfg.restservice.model.exception.ModelException;

public interface ImageService {

  String saveImage(MultipartFile file, Long id) throws ModelException;

  //ImageDTO getImage(String imagePath, Long id) throws ModelException;
}

