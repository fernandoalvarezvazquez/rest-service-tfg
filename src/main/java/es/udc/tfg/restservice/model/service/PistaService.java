package es.udc.tfg.restservice.model.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.udc.tfg.restservice.model.domain.Club;
import es.udc.tfg.restservice.model.domain.Partido;
import es.udc.tfg.restservice.model.domain.Pista;
import es.udc.tfg.restservice.model.exception.NotFoundException;
import es.udc.tfg.restservice.model.repository.ClubDao;
import es.udc.tfg.restservice.model.repository.PartidoDao;
import es.udc.tfg.restservice.model.repository.PistaDao;
import es.udc.tfg.restservice.model.service.dto.ClubDTO;
import es.udc.tfg.restservice.model.service.dto.PistaDTO;

@Service
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class PistaService {

	@Autowired
	private PistaDao pistaDAO;
	
	@Autowired
	private PartidoDao partidoDao;
	
	@Autowired
	private ClubDao clubDao;
	
	public List<PistaDTO> findAllByClub(Long idClub) throws NotFoundException{
		Club club1 = clubDao.findById(idClub);
		if (club1 == null)
			throw new NotFoundException(idClub.toString(), Club.class);
		return pistaDAO.findAllByClub(idClub).stream().map(pista -> new PistaDTO(pista)).collect(Collectors.toList());
	}
	
	public PistaDTO findById(Long id) throws NotFoundException{
		Pista pista = pistaDAO.findById(id);
		if (pista == null)
			throw new NotFoundException(id.toString(), Pista.class);
		return new PistaDTO(pista);
	}
	
	
	@PreAuthorize("hasAuthority('ADMIN_WEB')")
	@Transactional(readOnly = false)
	public PistaDTO create(PistaDTO pista) {
		Pista p = new Pista(clubDao.findById(pista.getClub()), pista.getNombre(), 
				pista.getTipoPista(), pista.getEstado(), pista.getPrecio());
		pistaDAO.create(p);
		return new PistaDTO(p);
	}
	
	@PreAuthorize("hasAnyAuthority('ADMIN_WEB', 'ADMIN_CLUB')")
	@Transactional(readOnly = false)
	public PistaDTO update(PistaDTO pista) {
		Pista bdPista = pistaDAO.findById(pista.getId());
		bdPista.setNombre((pista.getNombre()));
		bdPista.setEstado(pista.getEstado());
		bdPista.setPrecio(pista.getPrecio());
		pistaDAO.update(bdPista);
		return new PistaDTO(bdPista);
	}
	
	@PreAuthorize("hasAnyAuthority('ADMIN_WEB', 'ADMIN_CLUB')")
	@Transactional(readOnly = false)
	public void deleteById(Long id) throws NotFoundException{
		
		List<Partido> partidos = partidoDao.findAll();
		for(int j=0; j<partidos.size(); j++) {
			if (partidos.get(j).getPista().getId() == id)
				partidoDao.deleteById(partidos.get(j).getId());
		}
		
		Pista pista = pistaDAO.findById(id);
		if (pista == null)
			throw new NotFoundException(id.toString(), Pista.class);
		
		pistaDAO.delete(pista);
	}
}















