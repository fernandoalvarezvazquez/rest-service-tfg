package es.udc.tfg.restservice.model.repository;

import java.util.List;

import es.udc.tfg.restservice.model.domain.BonoClases;

public interface BonoClasesDao {
	
	List<BonoClases> findAll();
	
	List<BonoClases> findAllByClub(Long idClub);
	
	BonoClases findById(Long id);
	
	List<BonoClases> findByJugador(Long idUser);
	
	void deleteById(Long id);
	
	void create(BonoClases bono);
	
	void update(BonoClases bono);
	
	void delete(BonoClases bono);
}
