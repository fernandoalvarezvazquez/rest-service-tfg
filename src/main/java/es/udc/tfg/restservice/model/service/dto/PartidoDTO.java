package es.udc.tfg.restservice.model.service.dto;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.Size;

import es.udc.tfg.restservice.model.domain.Partido;
import es.udc.tfg.restservice.model.domain.Usuario;

public class PartidoDTO {

	private Long id;
	private String creador;
	private LocalDateTime hora_ini;
	private LocalDateTime hora_fin;
	private String resultado;
	private String resultadoSets;
	private boolean pagado;
	private double precioFinal;
	private Long idPista;
	
	@Size(max = 4)
	private List<String> jugadores = new ArrayList<String>();
	
	public PartidoDTO() {}
	
	public PartidoDTO(Partido partido) {
		this.id = partido.getId();
		this.creador = partido.getCreador().getLogin();
		this.hora_ini = partido.getHora_ini();
		this.hora_fin = partido.getHora_fin();
		this.resultado = partido.getResultado();
		this.resultadoSets = partido.getResultadoSets();
		if (partido.getPista() != null)
			this.setPista(partido.getPista().getId());
		for (int i = 0; i<partido.getJugadores().size(); i++) {
			this.jugadores.add(partido.getJugadores().get(i).getLogin());
		}
		this.precioFinal = partido.getPreciofinal();
		this.pagado = partido.isPagado();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCreador() {
		return creador;
	}

	public void setCreador(String creador) {
		this.creador = creador;
	}

	public LocalDateTime getHora_ini() {
		return hora_ini;
	}

	public void setHora_ini(LocalDateTime hora_ini) {
		this.hora_ini = hora_ini;
	}

	public LocalDateTime getHora_fin() {
		return hora_fin;
	}

	public void setHora_fin(LocalDateTime hora_fin) {
		this.hora_fin = hora_fin;
	}

	public String getResultado() {
		return resultado;
	}

	public void setResultado(String resultado) {
		this.resultado = resultado;
	}

	public String getResultadoSets() {
		return resultadoSets;
	}

	public void setResultadoSets(String resultadoSets) {
		this.resultadoSets = resultadoSets;
	}

	public boolean isPagado() {
		return pagado;
	}

	public void setPagado(boolean pagado) {
		this.pagado = pagado;
	}

	public double getPrecioFinal() {
		return precioFinal;
	}

	public void setPrecioFinal(double precioFinal) {
		this.precioFinal = precioFinal;
	}

	public Long getPista() {
		return idPista;
	}

	public void setPista(Long idPista) {
		this.idPista = idPista;
	}

	public List<String> getJugadores() {
		return jugadores;
	}

	public void setJugadores(List<String> jugadores) {
		this.jugadores = jugadores;
	}
	
	public void addJugadores(String name) {
		this.jugadores.add(name);
	}
	
	public void deleteJugadores(String name) {
		this.jugadores.remove(name);
	}
	
}
