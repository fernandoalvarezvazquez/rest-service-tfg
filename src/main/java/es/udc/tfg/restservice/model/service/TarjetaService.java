package es.udc.tfg.restservice.model.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.udc.tfg.restservice.model.domain.Tarjeta;
import es.udc.tfg.restservice.model.domain.Usuario;
import es.udc.tfg.restservice.model.exception.NotFoundException;
import es.udc.tfg.restservice.model.repository.TarjetaDao;
import es.udc.tfg.restservice.model.repository.UsuarioDao;
import es.udc.tfg.restservice.model.service.dto.TarjetaDTO;

@Service
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class TarjetaService {

	@Autowired
	private TarjetaDao tarjetaDao;
	
	@Autowired
	private UsuarioDao usuarioDao;
	
	public List<TarjetaDTO> findAll(){
		return tarjetaDao.findAll().stream().map(tarjeta -> new TarjetaDTO(tarjeta)).collect(Collectors.toList());
	}
	
	public TarjetaDTO findById(Long id) throws NotFoundException{
		Tarjeta tarjeta = tarjetaDao.findById(id);
		if (tarjeta == null)
			throw new NotFoundException(id.toString(), Tarjeta.class);
		return new TarjetaDTO(tarjetaDao.findById(id));
	}
	
	@PreAuthorize("isAuthenticated()")
	@Transactional(readOnly = false)
	public List<TarjetaDTO> findByPlayer(Long id) throws NotFoundException{
		Usuario usuario = usuarioDao.findById(id);
		if (usuario == null) {
			throw new NotFoundException(id.toString(), Usuario.class);
		}
		return tarjetaDao.findByPlayer(id).stream().map(tarjeta -> new TarjetaDTO(tarjeta)).collect(Collectors.toList());
	}
	
	@PreAuthorize("isAuthenticated()")
	@Transactional(readOnly = false)
	public TarjetaDTO create(TarjetaDTO tarjeta) {
		Tarjeta t = new Tarjeta(tarjeta.getTitular(), tarjeta.getNumero(), tarjeta.getCaducidad(), usuarioDao.findById(tarjeta.getJugadorId()));
		tarjetaDao.create(t);
		return new TarjetaDTO(t);
	}
	
	@PreAuthorize("isAuthenticated()")
	@Transactional(readOnly = false)
	public TarjetaDTO update (TarjetaDTO tarjeta) {
		Tarjeta bdTarjeta = tarjetaDao.findById(tarjeta.getId());
		bdTarjeta.setTitular(tarjeta.getTitular());
		bdTarjeta.setNumero(tarjeta.getNumero());
		bdTarjeta.setCaducidad(tarjeta.getCaducidad());
		tarjetaDao.update(bdTarjeta);
		return new TarjetaDTO(bdTarjeta);
	}
	
	@PreAuthorize("isAuthenticated()")
	@Transactional(readOnly = false)
	public void deleteById(Long id) throws NotFoundException{
		Tarjeta tarjeta = tarjetaDao.findById(id);
		if(tarjeta==null)
			throw new NotFoundException(id.toString(), Tarjeta.class);
		tarjetaDao.delete(tarjeta);
	}
}
