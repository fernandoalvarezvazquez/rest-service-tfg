package es.udc.tfg.restservice.model.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.multipart.MultipartFile;

import es.udc.tfg.restservice.model.domain.BonoClases;
import es.udc.tfg.restservice.model.domain.Club;
import es.udc.tfg.restservice.model.domain.Partido;
import es.udc.tfg.restservice.model.domain.Pista;
import es.udc.tfg.restservice.model.domain.Usuario;
import es.udc.tfg.restservice.model.exception.NotFoundException;
import es.udc.tfg.restservice.model.repository.BonoClasesDao;
import es.udc.tfg.restservice.model.repository.ClubDao;
import es.udc.tfg.restservice.model.repository.PartidoDao;
import es.udc.tfg.restservice.model.repository.PistaDao;
import es.udc.tfg.restservice.model.repository.UsuarioDao;
import es.udc.tfg.restservice.model.service.dto.BonoClasesDTO;
import es.udc.tfg.restservice.model.service.dto.ClubDTO;
import es.udc.tfg.restservice.web.exceptions.IdAndBodyNotMatchingOnUpdateException;
import es.udc.tfg.restservice.web.exceptions.RequestBodyNotValidException;

@Service
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class ClubService {
	
	@Autowired
	private ClubDao clubDAO;
	
	@Autowired
	private UsuarioDao usuarioDao;
	
	@Autowired
	private PistaDao pistaDao;
	
	@Autowired
	private PartidoDao partidoDao;
	
	@Autowired
	private BonoClasesDao bonoDao;
	
	public List<ClubDTO> findAll(){
		return clubDAO.findAll().stream().map(club -> new ClubDTO(club)).collect(Collectors.toList());
	}

	public ClubDTO findById(Long id) throws NotFoundException{
		Club club = clubDAO.findById(id);
		if (club == null) {
			throw new NotFoundException(id.toString(), Club.class);
		}
		return new ClubDTO(clubDAO.findById(id));
	}
	
	public List<ClubDTO> findByName(String name) {
		return clubDAO.findByName(name).stream().map(club -> new ClubDTO(club)).collect(Collectors.toList());	
	}
	
	public ClubDTO findByAdmin(Long idAdmin) throws NotFoundException{
		Usuario user = usuarioDao.findById(idAdmin);
		if (user==null) {
			throw new NotFoundException(idAdmin.toString(), Usuario.class);
		}
		return new ClubDTO(clubDAO.findByAdmin(user));
	}
	
	@PreAuthorize("hasAuthority('ADMIN_WEB')")
	@Transactional(readOnly = false)
	public ClubDTO create(ClubDTO club) {
		Club c = new Club(club.getNombre(), club.getDescripcion(), club.getDireccion(),
				club.getTelefono(), club.getSiglas(), usuarioDao.findByLogin(club.getAdminClub()), club.getImagenPath());
		clubDAO.create(c);
		return new ClubDTO(c);
	}
	
	@PreAuthorize("hasAnyAuthority('ADMIN_WEB', 'ADMIN_CLUB')")
	@Transactional(readOnly = false)
	public ClubDTO update(ClubDTO club) {
		Club bdClub = clubDAO.findById(club.getId());
		Usuario adminClub = usuarioDao.findByLogin(club.getAdminClub());
		bdClub.setNombre(club.getNombre());
		bdClub.setDireccion(club.getDireccion());
		bdClub.setDescripcion(club.getDescripcion());
		bdClub.setTelefono(club.getTelefono());
		bdClub.setSiglas(club.getSiglas());
		bdClub.setAdminClub(adminClub);
		bdClub.setImagenPath(club.getImagenPath());
		clubDAO.update(bdClub);
		return new ClubDTO(bdClub);
	}
	
	public String saveImage(Long id, MultipartFile file) throws IOException, NotFoundException {
		Club club = clubDAO.findById(id);
        
		if (club == null) {
        	throw new NotFoundException(id.toString(), Club.class);
        }
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        Path path = Paths.get("uploads/" + fileName);
        Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
        
        club.setImagenPath(path.toString());
        clubDAO.update(club);
        
        return path.toString();
    }
	
	@PreAuthorize("hasAuthority('ADMIN_WEB')")
	@Transactional(readOnly = false)
	public void deleteById(Long id) throws NotFoundException{
		Club club = clubDAO.findById(id);
		
		List<Pista> pistas = pistaDao.findAllByClub(id);
		
		List<Partido> partidos = partidoDao.findAll();
		
		List<BonoClases> bonos = bonoDao.findAllByClub(id);
		
		for(int i = 0; i<pistas.size(); i++) {
			for(int j=0; j<partidos.size(); j++) {
				if(partidos.get(j).getPista().getId() == pistas.get(i).getId()) {
					partidoDao.deleteById(partidos.get(j).getId());
				}
			}
			pistaDao.deleteById(pistas.get(i).getId());
		}
		
		for (int i = 0; i<bonos.size(); i++) {
			bonoDao.deleteById(bonos.get(i).getId());
		}
		
		if (club==null) {
			throw new NotFoundException(id.toString(), Club.class);
		}
		clubDAO.delete(club);
	}
	
}






