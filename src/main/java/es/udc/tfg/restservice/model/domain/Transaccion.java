package es.udc.tfg.restservice.model.domain;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class Transaccion {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "trans_generator")
	@SequenceGenerator(name = "trans_generator", sequenceName = "trans_seq")
	@Column(name = "id_trans")
	private Long id;
	
	private String orderId;

    private String cantidad;
    
    private String moneda;

    private LocalDateTime fecha;

    @Enumerated(EnumType.STRING)
    private PaymentStatus status;

    @ManyToOne
    private Tarjeta tarjeta;

    public Transaccion() {}
    
    public Transaccion(String orderId, String cantidad, String moneda, LocalDateTime fecha, Tarjeta tarjeta) {
    	this.orderId = orderId;
    	this.cantidad = cantidad;
    	this.moneda = moneda;
    	this.fecha = fecha;
    	this.tarjeta = tarjeta;
    }

    public enum PaymentStatus {
        PENDIENTE,
        COMPLETADO,
        FALLIDA,
        REINGRESADO
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getCantidad() {
		return cantidad;
	}

	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public LocalDateTime getFecha() {
		return fecha;
	}

	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}

	public PaymentStatus getStatus() {
		return status;
	}

	public void setStatus(PaymentStatus status) {
		this.status = status;
	}

	public Tarjeta getTarjeta() {
		return tarjeta;
	}

	public void setTarjeta(Tarjeta tarjeta) {
		this.tarjeta = tarjeta;
	}
}
