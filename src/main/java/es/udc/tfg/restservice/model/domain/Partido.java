package es.udc.tfg.restservice.model.domain;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;


@Entity
@Table(uniqueConstraints={
	    @UniqueConstraint(columnNames = {"pista_partido", "hora_ini"})
	})
public class Partido {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "part_generator")
	@SequenceGenerator(name = "part_generator", sequenceName = "part_seq")
	@Column(name = "id_partido")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "creador_part", nullable = false)
	private Usuario creador;
	
	@Column(nullable = false)
	private LocalDateTime hora_ini;
	
	@Column(nullable = false)
	private LocalDateTime hora_fin;
	
	private String resultado;
	
	private String resultadoSets;
	
	private double preciofinal;
	
	private boolean pagado;
	
	@ManyToOne
	@JoinColumn(name = "pista_partido", nullable = false)
	private Pista pista;
	
	@ManyToMany
	@JoinTable(name = "t_part_jug",
	  joinColumns = @JoinColumn(name = "id_partido"),
	  inverseJoinColumns = @JoinColumn(name = "id_usuario"))
	@Size(max = 4)
	private List<Usuario> jugadores;
	
	public Partido() {}
	
	public Partido(Usuario creador, LocalDateTime hora_ini, LocalDateTime hora_fin,
			Pista pista, boolean pagado, double precioFinal, String resultado, String resultadoSets){
		this.creador = creador;
		this.hora_ini = hora_ini;
		this.hora_fin = hora_fin;
		this.pista = pista;
		this.jugadores = new ArrayList<Usuario>();
		if (creador.getAutoridad() == UserAuthority.PLAYER)
			jugadores.add(creador);
		this.pagado = pagado;
		this.preciofinal = precioFinal;
		this.resultado = resultado;
		this.resultadoSets = resultadoSets;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public LocalDateTime getHora_ini() {
		return hora_ini;
	}

	public void setHora_ini(LocalDateTime hora_ini) {
		this.hora_ini = hora_ini;
	}

	public LocalDateTime getHora_fin() {
		return hora_fin;
	}

	public void setHora_fin(LocalDateTime hora_fin) {
		this.hora_fin = hora_fin;
	}


	public Usuario getCreador() {
		return creador;
	}

	public void setCreador(Usuario creador) {
		this.creador = creador;
	}

	public String getResultado() {
		return resultado;
	}

	public void setResultado(String resultado) {
		this.resultado = resultado;
	}

	public String getResultadoSets() {
		return resultadoSets;
	}

	public void setResultadoSets(String resultadoSets) {
		this.resultadoSets = resultadoSets;
	}

	public double getPreciofinal() {
		return preciofinal;
	}

	public void setPreciofinal(double preciofinal) {
		this.preciofinal = preciofinal;
	}

	public boolean isPagado() {
		return pagado;
	}

	public void setPagado(boolean pagado) {
		this.pagado = pagado;
	}

	public Pista getPista() {
		return pista;
	}

	public void setPista(Pista pista) {
		this.pista = pista;
	}

	public List<Usuario> getJugadores() {
		return jugadores;
	}

	public void setJugadores(List<Usuario> jugadores) {
		this.jugadores = jugadores;
	}
	
	public void addJugador(Usuario jugador) {
		boolean esta = false;
		for(int i = 0; i< this.jugadores.size(); i++)
			if (this.jugadores.get(i).getId() == jugador.getId())
				esta = true;
		
		if(this.jugadores.size() <  this.pista.getTipoPista().getMaxPersonas() && !esta)
			this.jugadores.add(jugador);
	}
	
	public void deleteJugador(Usuario jugador) {
		this.jugadores.remove(jugador);
	}
	
}
