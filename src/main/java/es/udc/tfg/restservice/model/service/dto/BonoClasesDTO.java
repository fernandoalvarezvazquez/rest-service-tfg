package es.udc.tfg.restservice.model.service.dto;

import es.udc.tfg.restservice.model.domain.BonoClases;

public class BonoClasesDTO {

	private Long id;
	private String nombre;
	private String descripcion;
	private Double precio;
	private int num_clases;
	private Long idClub;
	private Long idUser;
	
	public BonoClasesDTO() {}
	
	public BonoClasesDTO(BonoClases bono) {
		this.id = bono.getId();
		this.nombre = bono.getNombre();
		this.descripcion = bono.getDescripcion();
		this.precio = bono.getPrecio();
		this.num_clases = bono.getNum_clases();
		this.idClub = bono.getClub().getId();
		if(bono.getJugador() != null)
			this.idUser = bono.getJugador().getId();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Double getPrecio() {
		return precio;
	}

	public void setPrecio(Double precio) {
		this.precio = precio;
	}

	public int getNum_clases() {
		return num_clases;
	}

	public void setNum_clases(int num_clases) {
		this.num_clases = num_clases;
	}

	public Long getIdClub() {
		return idClub;
	}

	public void setIdClub(Long idClub) {
		this.idClub = idClub;
	}

	public Long getIdUser() {
		return idUser;
	}

	public void setIdUser(Long idUser) {
		this.idUser = idUser;
	}
	
	
}
