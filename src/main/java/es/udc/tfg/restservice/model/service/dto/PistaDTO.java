package es.udc.tfg.restservice.model.service.dto;

import es.udc.tfg.restservice.model.domain.Pista;
import es.udc.tfg.restservice.model.domain.TipoPista;

public class PistaDTO {

	private Long id;
	private String nombre;
	private TipoPista tipo;
	private Boolean estado;
	private double precio;
	private Long idClub;

	public PistaDTO() {}
	
	public PistaDTO(Pista pista) {
		this.id = pista.getId();
		this.nombre = pista.getNombre();
		this.tipo = pista.getTipoPista();
		this.estado = pista.getEstado();
		this.precio = pista.getPrecio();
		if (pista.getClub() != null) {
			this.setClub(pista.getClub().getId());
			pista.getClub().addPista(pista);
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public TipoPista getTipoPista() {
		return tipo;
	}

	public void setTipoPista(TipoPista tipo) {
		this.tipo = tipo;
	}

	public Boolean getEstado() {
		return estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public Long getClub() {
		return idClub;
	}

	public void setClub(Long idClub) {
		this.idClub = idClub;
	}
	
}
