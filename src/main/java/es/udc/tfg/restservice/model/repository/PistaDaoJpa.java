package es.udc.tfg.restservice.model.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import es.udc.tfg.restservice.model.domain.Club;
import es.udc.tfg.restservice.model.domain.Pista;
import es.udc.tfg.restservice.model.repository.util.GenericDaoJpa;

@Repository
public class PistaDaoJpa extends GenericDaoJpa implements PistaDao{

	@Override
	public List<Pista> findAllByClub(Long idClub) {
		return entityManager.createQuery("from Pista p where p.club.id = :id", Pista.class)
				.setParameter("id", idClub).getResultList();
	}

	@Override
	public Pista findById(Long id) {
		return entityManager.find(Pista.class, id);
	}
	
	@Override
	public Pista findByName(String nombre) {
		return entityManager.createQuery("from Pista p where p.nombre = :nombre", Pista.class)
				.setParameter("nombre", nombre).getSingleResult();
	}

	@Override
	public void deleteById(Long id) {
		Pista pista = findById(id);
		delete(pista);
	}

	@Override
	public void create(Pista pista) {
		entityManager.persist(pista);
	}

	@Override
	public void update(Pista pista) {
		entityManager.merge(pista);
	}

	@Override
	public void delete(Pista pista) {
		entityManager.remove(pista);
	}
	
}
