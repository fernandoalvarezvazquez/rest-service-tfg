package es.udc.tfg.restservice.model.service.dto;

import java.util.ArrayList;
import java.util.List;

import es.udc.tfg.restservice.model.domain.Club;
import es.udc.tfg.restservice.model.domain.Pista;

public class ClubDTO {
	
	private Long id;
	private String nombre;
	private String descripcion;
	private String direccion;
	private String telefono;
	private String siglas;
	private List<Pista> pistas = new ArrayList<>();
	private int num_pistas;
	private String adminClub;
	private String imagenPath;
	
	public ClubDTO() {}
	
	public ClubDTO(Club club){
		this.id = club.getId();
		this.nombre = club.getNombre();
		this.descripcion = club.getDescripcion();
		this.direccion = club.getDireccion();
		this.telefono = club.getTelefono();
		this.siglas = club.getSiglas();
		if (club.getAdminClub()!=null)
			this.setAdminClub(club.getAdminClub().getLogin());
		for(int i = 0; i<club.getPistas().size(); i++) {
			this.pistas.add(club.getPistas().get(i));
		}
		this.setNum_pistas(pistas.size()); 
		this.setImagenPath(club.getImagenPath());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getSiglas() {
		return siglas;
	}

	public void setSiglas(String siglas) {
		this.siglas = siglas;
	}

	public String getAdminClub() {
		return adminClub;
	}

	public void setAdminClub(String adminClub) {
		this.adminClub = adminClub;
	}

	public int getNum_pistas() {
		return num_pistas;
	}

	public void setNum_pistas(int num_pistas) {
		this.num_pistas = num_pistas;
	}

	public String getImagenPath() {
		return imagenPath;
	}

	public void setImagenPath(String imagen) {
		this.imagenPath = imagen;
	}
		
}
