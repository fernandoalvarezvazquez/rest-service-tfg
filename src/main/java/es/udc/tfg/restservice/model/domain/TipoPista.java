package es.udc.tfg.restservice.model.domain;

public enum TipoPista {
	PADEL_IND(2), 
	PADEL_DOBLES(4);
	
	int maxPersonas;
	
	private TipoPista(int maxPersonas) {
        this.maxPersonas = maxPersonas;
    }

    public int getMaxPersonas() {
        return maxPersonas;
    }
}
