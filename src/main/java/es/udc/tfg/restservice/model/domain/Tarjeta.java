package es.udc.tfg.restservice.model.domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
public class Tarjeta {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "card_generator")
	@SequenceGenerator(name = "card_generator", sequenceName = "card_seq")
	@Column(name = "id_card")
	private Long id;

	private String titular;
	
	@Column(unique = true)
	private String numero;
	
	private String ultimos4digitos;
	
	private String caducidad;
	
	@ManyToOne
	private Usuario jugador;
	
	@OneToMany(mappedBy = "tarjeta")
	private List<Transaccion> transaciones;
	
	public Tarjeta() {}
	
	public Tarjeta(String titular, String numero, String caducidad, Usuario jugador) {
		this.titular = titular;
		this.numero = numero;
		this.caducidad = caducidad;
		this.jugador = jugador;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getTitular() {
		return titular;
	}

	public void setTitular(String titular) {
		this.titular = titular;
	}

	public String getCaducidad() {
		return caducidad;
	}

	public void setCaducidad(String caducidad) {
		this.caducidad = caducidad;
	}

	public Usuario getJugador() {
		return jugador;
	}

	public void setJugador(Usuario jugador) {
		this.jugador = jugador;
	}

	public String getUltimos4digitos() {
		ultimos4digitos= "**** **** **** " + numero.substring(numero.length()-4);
		return ultimos4digitos;
	}

	public void setUltimos4digitos(String ultimos4digitos) {
		this.ultimos4digitos = ultimos4digitos;
	}

	public String getNumero() {
		return numero;
	}
	
	public void setNumero(String numero) {
		this.ultimos4digitos = numero;
	}
	
}
