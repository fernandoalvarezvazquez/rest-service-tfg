package es.udc.tfg.restservice.model.repository;

import java.util.List;

import es.udc.tfg.restservice.model.domain.Club;
import es.udc.tfg.restservice.model.domain.Pista;

public interface PistaDao {

	List<Pista> findAllByClub(Long idClub);
	
	Pista findById(Long id);
	
	Pista findByName(String name);
	
	void deleteById(Long id);

	void create(Pista pista);

	void update(Pista pista);

	void delete(Pista pista);
}
