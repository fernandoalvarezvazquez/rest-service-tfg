package es.udc.tfg.restservice.model.exception;

public class ModelException extends Exception {
  public ModelException(String msg) {
    super(msg);
  }
}
