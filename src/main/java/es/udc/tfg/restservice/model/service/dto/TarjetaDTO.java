package es.udc.tfg.restservice.model.service.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import es.udc.tfg.restservice.model.domain.Tarjeta;
import es.udc.tfg.restservice.model.domain.Usuario;

public class TarjetaDTO {
	
	private Long id;
	
	@NotNull
	@Size(max = 100)
	private String titular;
	
	private String numero;
	
	@Pattern(regexp = "\\d{4} \\d{4} \\d{4} \\d{4}")
	private String ultimos4numero;
	
	@Pattern(regexp = "\\d{2}/\\d{2}")
	private String caducidad;
	
	private Long jugadorId;
	
	public TarjetaDTO() {}
	
	public TarjetaDTO(Tarjeta tarjeta) {
		this.id = tarjeta.getId();
		this.titular = tarjeta.getTitular();
		//this.ultimos4numero = tarjeta.getUltimos4digitos();
		this.numero = tarjeta.getNumero();
		this.caducidad = tarjeta.getCaducidad();
		if (tarjeta.getJugador() != null)
			this.jugadorId = tarjeta.getJugador().getId();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitular() {
		return titular;
	}

	public void setTitular(String titular) {
		this.titular = titular;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getCaducidad() {
		return caducidad;
	}

	public void setCaducidad(String caducidad) {
		this.caducidad = caducidad;
	}

	public Long getJugadorId() {
		return jugadorId;
	}

	public void setJugador(Long jugadorId) {
		this.jugadorId = jugadorId;
	}

	public String getUltimos4numero() {
		return ultimos4numero;
	}

	public void setUltimos4numero(String ultimos4numero) {
		this.ultimos4numero = ultimos4numero;
	}

	public void setJugadorId(Long jugadorId) {
		this.jugadorId = jugadorId;
	}
	
	
}
