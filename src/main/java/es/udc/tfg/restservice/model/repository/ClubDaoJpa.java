package es.udc.tfg.restservice.model.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import es.udc.tfg.restservice.model.domain.Club;
import es.udc.tfg.restservice.model.domain.Usuario;
import es.udc.tfg.restservice.model.repository.util.GenericDaoJpa;

@Repository
public class ClubDaoJpa extends GenericDaoJpa implements ClubDao{

	@Override
	public List<Club> findAll() {
		return entityManager.createQuery("from Club", Club.class). getResultList();
	}

	@Override
	public Club findById(Long id) {
		return entityManager.find(Club.class, id);
	}

	@Override
	public List<Club> findByName(String nombre) {
		return entityManager.createQuery("from Club c where lower(c.nombre) like lower(:nombre)", Club.class)
				.setParameter("nombre", "%"+nombre+"%").getResultList();
	}

	@Override
	public Club findByAdmin(Usuario user) {
		return entityManager.createQuery("from Club c where c.adminClub = :user", Club.class)
				.setParameter("user", user).getSingleResult();
	}
	
	@Override
	public void deleteById(Long id) {
		Club club = findById(id);
		delete(club);
	}

	@Override
	public void create(Club club) {
		entityManager.persist(club);
	}

	@Override
	public void update(Club club) {
		entityManager.merge(club);
	}

	@Override
	public void delete(Club club) {
		entityManager.remove(club);
	}

}
