package es.udc.tfg.restservice.model.service.dto;

import es.udc.tfg.restservice.model.domain.UserAuthority;
import es.udc.tfg.restservice.model.domain.Usuario;

public class UserDTOPublic {
  private Long id;
  private String login;
  private String contrasena;
  private UserAuthority authority;
  private double nivel;
  private String fotoPerfil;

  public UserDTOPublic() {
  }

  public UserDTOPublic(Usuario user) {
    this.id = user.getId();
    this.login = user.getLogin();
    this.authority = user.getAutoridad();
    if (user.getNivel() != 0.0)
    	this.nivel = user.getNivel();
    else this.nivel = 0.0;
    this.fotoPerfil = user.getFotoPerfil();
  }
  

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

public UserAuthority getAuthority() {
	return authority;
}

public void setAuthority(UserAuthority authority) {
	this.authority = authority;
}

public String getContrasena() {
	return contrasena;
}

public void setContrasena(String contrasena) {
	this.contrasena = contrasena;
}

public double getNivel() {
	return nivel;
}

public void setNivel(double nivel) {
	this.nivel = nivel;
}

public String getFotoPerfil() {
	return fotoPerfil;
}

public void setFotoPerfil(String fotoPerfil) {
	this.fotoPerfil = fotoPerfil;
}

}
