package es.udc.tfg.restservice.model.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import es.udc.tfg.restservice.model.domain.Tarjeta;
import es.udc.tfg.restservice.model.domain.Transaccion;
import es.udc.tfg.restservice.model.repository.util.GenericDaoJpa;

@Repository
public class TransaccionDaoJpa extends GenericDaoJpa implements TransaccionDao{

	@Override
	public List<Transaccion> findAll() {
		return entityManager.createQuery("from Transaccion", Transaccion.class).getResultList();
	}

	@Override
	public Transaccion findById(Long id) {
		return entityManager.find(Transaccion.class, id);
	}

	@Override
	public void deleteById(Long id) {
		Transaccion t = findById(id);
		delete(t);
	}

	@Override
	public void create(Transaccion transaccion) {
		entityManager.persist(transaccion);
	}

	@Override
	public void update(Transaccion transaccion) {
		entityManager.merge(transaccion);
	}

	@Override
	public void delete(Transaccion transaccion) {
		entityManager.remove(transaccion);
	}
	
}
