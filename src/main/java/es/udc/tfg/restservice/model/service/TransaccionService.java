package es.udc.tfg.restservice.model.service;

import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.paypal.base.rest.APIContext;

import es.udc.tfg.restservice.model.domain.Transaccion;
import es.udc.tfg.restservice.model.exception.NotFoundException;
import es.udc.tfg.restservice.model.repository.TransaccionDao;
import es.udc.tfg.restservice.model.service.dto.TransaccionDTO;

@Service
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class TransaccionService {
	
	@Autowired
    private APIContext apiContext;

    @Autowired
    private TransaccionDao transaccionDao;
    
    @PreAuthorize("isAuthenticated()")
    public List<TransaccionDTO> findAll(){
    	return transaccionDao.findAll().stream().map(transaccion -> new TransaccionDTO(transaccion)).collect(Collectors.toList());
    }
    
    @PreAuthorize("isAuthenticated()")
    public TransaccionDTO findById(Long id) throws NotFoundException{
    	Transaccion t = transaccionDao.findById(id);
    	if (t == null) {
    		throw new NotFoundException(id.toString(), Transaccion.class);
    	}
    	return new TransaccionDTO(t);
    }
    
    @PreAuthorize("isAuthenticated()")
    @Transactional(readOnly = false)
    public TransaccionDTO create(TransaccionDTO tran) {
    	Transaccion t = new Transaccion(tran.getOrderId(), tran.getCantidad(), tran.getMoneda(), tran.getFecha(), tran.getTarjeta());
    	transaccionDao.create(t);
    	return new TransaccionDTO(t);
    }
    
    @PreAuthorize("isAuthenticated()")
    @Transactional(readOnly = false)
    public TransaccionDTO cambiarEstado(TransaccionDTO tran){
    	Transaccion bdTran = transaccionDao.findById(tran.getId());
    	bdTran.setStatus(tran.getStatus());
    	transaccionDao.update(bdTran);
    	return new TransaccionDTO(bdTran);
    }

}







