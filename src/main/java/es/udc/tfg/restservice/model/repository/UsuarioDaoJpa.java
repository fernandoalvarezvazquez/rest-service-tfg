package es.udc.tfg.restservice.model.repository;

import java.util.List;

import javax.persistence.*;

import org.springframework.dao.support.DataAccessUtils;
import org.springframework.stereotype.Repository;

import es.udc.tfg.restservice.model.domain.Partido;
import es.udc.tfg.restservice.model.domain.Tarjeta;
import es.udc.tfg.restservice.model.domain.Usuario;
import es.udc.tfg.restservice.model.repository.util.GenericDaoJpa;


@Repository
public class UsuarioDaoJpa extends GenericDaoJpa implements UsuarioDao{
  @Override
  public List<Usuario> findAll() {
    return entityManager.createQuery("from Usuario", Usuario.class).getResultList();
  }
  
  @Override
  public List<Usuario> findAllJugadores(){
	  return entityManager.createQuery("from Usuario u where u.autoridad = 'PLAYER'", Usuario.class).getResultList();
  }

  @Override
  public Usuario findById(Long id) {
    return entityManager.find(Usuario.class, id);
  }

  @Override
  public Usuario findByLogin(String login) {
    TypedQuery<Usuario> query = entityManager.createQuery("from Usuario u where u.login = :login", Usuario.class)
      .setParameter("login", login);
    return DataAccessUtils.singleResult(query.getResultList());
  }
  
  @Override
  public List<Partido> findPartidos(Long id){
	  return entityManager.createQuery("SELECT p FROM Partido p JOIN p.jugadores j WHERE j.id = :id order by p.hora_ini DESC", Partido.class)
		        .setParameter("id", id).getResultList();
  }
  
  @Override
  public List<Tarjeta> findTarjetas(Long id){
	  return entityManager.createQuery("from Tarjeta t where t.jugador.id = :id", Tarjeta.class)
			  .setParameter("id", id).getResultList();
  }

  /*@Override
  public List<Producto> findAllCompras(Long id) {
    return entityManager.createQuery("from Producto p where p.comprador.id = :id", Producto.class)
      .setParameter("id", id).getResultList();
  }

  @Override
  public List<Producto> findAllVentas(Long id) {
    return entityManager.createQuery("from Producto p where p.vendedor.id = :id", Producto.class)
      .setParameter("id", id).getResultList();
  }

  @Override
  public List<Producto> findAllMeGusta(Long id) {
    return entityManager.createQuery("Select p from Producto p inner join p.usuariosList mg where mg.id = :id", Producto.class)
      .setParameter("id", id).getResultList();
  }

  @Override
  public List<Mensaje> findAllMensaje(Long id) {
    return entityManager.createQuery("select distinct m from Mensaje m  " +
                                              "where (m.userEnvio.id = :id or m.userRecibo.id = :id)", Mensaje.class)
      .setParameter("id", id).getResultList();
  }

  @Override
  public List<Mensaje> findConversacion(Long idEnvia, Long idRecibe) {
    return entityManager.createQuery("from Mensaje m " +
                                              "where (m.userEnvio.id = :idEnvia or m.userEnvio.id = :idRecibe) " +
                                              "and (m.userRecibo.id = :idEnvia or m.userRecibo.id = :idRecibe)", Mensaje.class)
      .setParameter("idEnvia", idEnvia).setParameter("idRecibe", idRecibe).getResultList();
  }

  @Override
  public List<Mensaje> findAllMensajeById(Long id) {
    return entityManager.createQuery("from Mensaje m " +
                                              "where (m.userEnvio.id = :id) or (m.userRecibo.id = :id)", Mensaje.class)
      .setParameter("id", id).getResultList();
  }*/

  @Override
  public void deleteById(Long id) {
    Usuario user = findById(id);
    delete(user);
  }
  
  @Override
  public void create(Usuario user) {
    entityManager.persist(user);
  }

  @Override
  public void update(Usuario user) {
    entityManager.merge(user);
  }
  
  @Override
  public void delete(Usuario user) {
    entityManager.remove(user);
  }

  
}
