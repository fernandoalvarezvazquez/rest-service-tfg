package es.udc.tfg.restservice.model.service;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.udc.tfg.restservice.model.domain.Club;
import es.udc.tfg.restservice.model.domain.Partido;
import es.udc.tfg.restservice.model.domain.Pista;
import es.udc.tfg.restservice.model.domain.Usuario;
import es.udc.tfg.restservice.model.exception.NotFoundException;
import es.udc.tfg.restservice.model.exception.OperationNotAllowed;
import es.udc.tfg.restservice.model.repository.ClubDao;
import es.udc.tfg.restservice.model.repository.PartidoDao;
import es.udc.tfg.restservice.model.repository.PistaDao;
import es.udc.tfg.restservice.model.repository.UsuarioDao;
import es.udc.tfg.restservice.model.service.dto.PartidoDTO;
import es.udc.tfg.restservice.model.service.dto.UserDTOPublic;

@Service
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class PartidoService {

	@Autowired
	private PartidoDao partidoDao;
	
	@Autowired
	private UsuarioDao usuarioDao;
	
	@Autowired
	private PistaDao pistaDao;
	
	@Autowired
	private ClubDao clubDao;
	
	@Autowired
	UsuarioService usuarioService;
	
	public List<PartidoDTO> findAll(){
		return partidoDao.findAll().stream().map(partido -> new PartidoDTO(partido)).collect(Collectors.toList());
	}
	
	public PartidoDTO findById(Long id) throws NotFoundException{
		Partido partido = partidoDao.findById(id);
		if (partido == null)
			throw new NotFoundException(id.toString(), Partido.class);
		return new PartidoDTO(partido);
	}
	
	public PartidoDTO findByPistaHora(Long idPista, LocalDateTime hora) throws NotFoundException{
		
		if(pistaDao.findById(idPista) == null)
			throw new NotFoundException(idPista.toString(), Pista.class);
		
		Partido p = partidoDao.findByPistaHora(idPista, hora);
		
		return new PartidoDTO(p);
		
	}
	
	public List<PartidoDTO> findByClub(Long idClub) throws NotFoundException{
		Club club = clubDao.findById(idClub);
		if (club == null)
			throw new NotFoundException(idClub.toString(), Club.class);
		return partidoDao.findByClub(idClub).stream().map(partido -> new PartidoDTO(partido)).collect(Collectors.toList());
		
	}
	
	@PreAuthorize("hasAnyAuthority('ADMIN_CLUB', 'PLAYER')")
	@Transactional(readOnly = false)
	public PartidoDTO create(PartidoDTO partido) {
		Pista pista = pistaDao.findById(partido.getPista());
		long duracion = Duration.between(partido.getHora_ini(), partido.getHora_fin()).toMinutes();
		if (duracion <= 35) {
			partido.setPrecioFinal(pista.getPrecio()/2);
		} else if (duracion <= 65) {
			partido.setPrecioFinal(pista.getPrecio());
		} else if (duracion <= 95) {
			partido.setPrecioFinal(pista.getPrecio()*1.5);
		} else {
			partido.setPrecioFinal(pista.getPrecio()*2);
		} 
		
		
		Partido p = new Partido(usuarioDao.findByLogin(partido.getCreador()), partido.getHora_ini(), partido.getHora_fin(),
				pistaDao.findById(partido.getPista()), partido.isPagado(), partido.getPrecioFinal(), "", "");
		partidoDao.create(p);
		return new PartidoDTO(p);
	}
	
	@PreAuthorize("hasAnyAuthority('ADMIN_CLUB','PLAYER')")
	@Transactional(readOnly = false)
	public PartidoDTO update(PartidoDTO partido) {
		Partido bdPartido = partidoDao.findById(partido.getId());
		bdPartido.setHora_ini(partido.getHora_ini());
		bdPartido.setHora_fin(partido.getHora_fin());
		bdPartido.setPista(pistaDao.findById(partido.getPista()));
		bdPartido.setJugadores(new ArrayList<>());
		for(int i = 0; i < partido.getJugadores().size(); i++) {
			bdPartido.addJugador(usuarioDao.findByLogin(partido.getJugadores().get(i)));
		}
		partidoDao.update(bdPartido);
		return new PartidoDTO(bdPartido);
	}
	
	@PreAuthorize("hasAnyAuthority('ADMIN_CLUB','PLAYER')")
	@Transactional(readOnly = false)
	public PartidoDTO addJugador(PartidoDTO partido, Usuario jugador) {
		Partido bdPartido = partidoDao.findById(partido.getId());
		bdPartido.addJugador(jugador);
		partidoDao.update(bdPartido);
		return new PartidoDTO(bdPartido);
	}
	
	@PreAuthorize("hasAnyAuthority('ADMIN_CLUB','PLAYER')")
	@Transactional(readOnly = false)
	public PartidoDTO deleteJugador(PartidoDTO partido, Usuario jugador){
		Partido bdPartido = partidoDao.findById(partido.getId());
		bdPartido.deleteJugador(usuarioDao.findById(jugador.getId()));
		partidoDao.update(bdPartido);
		return new PartidoDTO(bdPartido);
	}
	
	@PreAuthorize("hasAuthority('PLAYER')")
	@Transactional(readOnly = false)
	public PartidoDTO updateResultado(PartidoDTO partido) throws NotFoundException {
		int setsA = 0;
		int setsB = 0;
		String resultadoSets = "";
		Partido bdPartido = partidoDao.findById(partido.getId());
		bdPartido.setResultado(partido.getResultado());
		
		if (partido.getJugadores().size() == 4) {
			if(resultadoSets.charAt(0) == 2) {
				List<String> parejaGanadora = partido.getJugadores().subList(0,1);
				List<String> parejaPerdedora = partido.getJugadores().subList(2,3);
				
				UserDTOPublic user1 = usuarioService.findByLogin(parejaGanadora.get(0));
				UserDTOPublic user2 = usuarioService.findByLogin(parejaGanadora.get(1));
				UserDTOPublic user3 = usuarioService.findByLogin(parejaPerdedora.get(0));
				UserDTOPublic user4 = usuarioService.findByLogin(parejaPerdedora.get(1));
				
				user1.setNivel(user1.getNivel()+0.12);
				user2.setNivel(user2.getNivel()+0.12);
				user3.setNivel(user3.getNivel()-0.12);
				user4.setNivel(user4.getNivel()-0.12);
				
				usuarioService.updateNivel(user1);
				usuarioService.updateNivel(user2);
				usuarioService.updateNivel(user3);
				usuarioService.updateNivel(user4);
				
			}
			else if(resultadoSets.charAt(1) == 2) {
				List<String> parejaPerdedora = partido.getJugadores().subList(0,1);
				List<String> parejaGanadora = partido.getJugadores().subList(2,3);
				
				UserDTOPublic user1 = usuarioService.findByLogin(parejaGanadora.get(0));
				UserDTOPublic user2 = usuarioService.findByLogin(parejaGanadora.get(1));
				UserDTOPublic user3 = usuarioService.findByLogin(parejaPerdedora.get(0));
				UserDTOPublic user4 = usuarioService.findByLogin(parejaPerdedora.get(1));
				
				user1.setNivel(user1.getNivel()+0.12);
				user2.setNivel(user2.getNivel()+0.12);
				user3.setNivel(user3.getNivel()-0.12);
				user4.setNivel(user4.getNivel()-0.12);
				
				usuarioService.updateNivel(user1);
				usuarioService.updateNivel(user2);
				usuarioService.updateNivel(user3);
				usuarioService.updateNivel(user4);
			}
			
		}
		
		
		//partido.setResultadoSets(resultadoSets);
		bdPartido.setResultadoSets(partido.getResultadoSets());
		partidoDao.update(bdPartido);
		return new PartidoDTO(bdPartido);
	}
	
	@PreAuthorize("isAuthenticated()")
	@Transactional(readOnly = false)
	public PartidoDTO updatePagado(PartidoDTO partido) {
		Partido bdPartido = partidoDao.findById(partido.getId());
		bdPartido.setPagado(partido.isPagado());
		partidoDao.update(bdPartido);
		return new PartidoDTO(bdPartido);
	}
	
	@PreAuthorize("isAuthenticated()")
	@Transactional(readOnly = false)
	public void deleteById(Long id) throws NotFoundException, OperationNotAllowed{
		Partido partido = partidoDao.findById(id);
		if (partido == null)
			throw new NotFoundException(id.toString(), Partido.class);
		partidoDao.deleteById(id);
	}

	
	
}
