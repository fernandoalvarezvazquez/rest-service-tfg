package es.udc.tfg.restservice.model.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class Usuario {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_generator")
	@SequenceGenerator(name = "user_generator", sequenceName = "user_seq")
	@Column(name = "id_usuario")
	private Long id;

	@Column(unique = true)
	private String login;

	private String contrasena;

	@Enumerated(EnumType.STRING)
	private UserAuthority autoridad;

	private double nivel;
	
	private String fotoPerfil;

	@OneToMany(mappedBy = "jugador")
	private List<Tarjeta> tarjetas = new ArrayList<Tarjeta>();

	@OneToOne(mappedBy = "adminClub")
	private Club miClub;

	@OneToMany(mappedBy = "creador")
	private List<Partido> partidos;

	@ManyToMany(mappedBy = "jugadores")
	private List<Partido> misPartidos;

	@OneToMany(mappedBy = "jugador")
	private List<BonoClases> bonos;

	@ManyToMany
	@JoinTable(name = "t_jug_club_fav", joinColumns = @JoinColumn(name = "id_usuario"), inverseJoinColumns = @JoinColumn(name = "id_club"))
	private List<Club> clubesFav;
	
	public Usuario() {
	}

	public Usuario(String login, String contrasena, UserAuthority autoridad, double nivel, String fotoPerfil) {
		this.login = login;
		this.contrasena = contrasena;
		this.autoridad = autoridad;
		this.nivel = nivel;
		this.fotoPerfil = fotoPerfil;
	}

	public Usuario(String login, String contrasena, UserAuthority autoridad, double nivel, String fotoPerfil, Club miClub) {
		this(login, contrasena, autoridad, nivel, fotoPerfil);
		this.miClub = miClub;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getContrasena() {
		return contrasena;
	}

	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}

	public UserAuthority getAutoridad() {
		return autoridad;
	}

	public void setAutoridad(UserAuthority autoridad) {
		this.autoridad = autoridad;
	}

	public double getNivel() {
		return nivel;
	}

	public void setNivel(double nivel) {
		this.nivel = nivel;
	}

	public String getFotoPerfil() {
		return fotoPerfil;
	}

	public void setFotoPerfil(String fotoPerfil) {
		this.fotoPerfil = fotoPerfil;
	}

	public List<Tarjeta> getTarjetas() {
		return tarjetas;
	}

	public void setTarjetas(List<Tarjeta> tarjetas) {
		this.tarjetas = tarjetas;
	}

	public Club getMiClub() {
		return miClub;
	}

	public void setMiClub(Club miClub) {
		this.miClub = miClub;
	}

	public List<Partido> getPartidos() {
		return partidos;
	}

	public void setPartidos(List<Partido> partidos) {
		this.partidos = partidos;
	}

	public void anadirPartido(Partido p) {
		this.partidos.add(p);
	}

	public List<Partido> getMisPartidos() {
		return misPartidos;
	}

	public void setMisPartidos(List<Partido> misPartidos) {
		this.misPartidos = misPartidos;
	}

	public void addMisPartidos(Partido p) {
		this.misPartidos.add(p);
	}

	public List<BonoClases> getBonos() {
		return bonos;
	}

	public void setBonos(List<BonoClases> bonos) {
		this.bonos = bonos;
	}

	public List<Club> getClubesFav() {
		return clubesFav;
	}

	public void setClubesFav(List<Club> clubesFav) {
		this.clubesFav = clubesFav;
	}
}
