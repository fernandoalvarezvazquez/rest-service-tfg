package es.udc.tfg.restservice.model.service.dto;

import java.time.LocalDateTime;

import es.udc.tfg.restservice.model.domain.Tarjeta;
import es.udc.tfg.restservice.model.domain.Transaccion;
import es.udc.tfg.restservice.model.domain.Transaccion.PaymentStatus;

public class TransaccionDTO {
	
	private Long id;
    private String orderId;
    private String cantidad;
    private String moneda;
    private LocalDateTime fecha;
    private PaymentStatus status;
    private Tarjeta tarjeta;
    
    public TransaccionDTO() {}
    
    public TransaccionDTO(Transaccion transaccion) {
    	this.id = transaccion.getId();
    	this.orderId = transaccion.getOrderId();
    	this.cantidad = transaccion.getCantidad();
    	this.moneda = transaccion.getMoneda();
    	this.fecha = transaccion.getFecha();
    	this.status = transaccion.getStatus();
    	this.tarjeta = transaccion.getTarjeta();
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getCantidad() {
		return cantidad;
	}

	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public LocalDateTime getFecha() {
		return fecha;
	}

	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}

	public PaymentStatus getStatus() {
		return status;
	}

	public void setStatus(PaymentStatus status) {
		this.status = status;
	}

	public Tarjeta getTarjeta() {
		return tarjeta;
	}

	public void setTarjeta(Tarjeta tarjeta) {
		this.tarjeta = tarjeta;
	}
    
}
