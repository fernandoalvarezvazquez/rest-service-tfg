package es.udc.tfg.restservice.model.domain;

public enum UserAuthority {
  // Definimos tres authorities o roles para los usuarios del sistema
  PLAYER, ADMIN_CLUB, ADMIN_WEB
}
