package es.udc.tfg.restservice.model.repository;

import java.util.List;

import es.udc.tfg.restservice.model.domain.Partido;
import es.udc.tfg.restservice.model.domain.Tarjeta;
import es.udc.tfg.restservice.model.domain.Usuario;

public interface UsuarioDao {

	  List<Usuario> findAll();
	  
	  List<Usuario> findAllJugadores();

	  Usuario findById(Long id);

	  Usuario findByLogin(String login);
	  
	  List<Partido> findPartidos(Long id);
	  
	  List<Tarjeta> findTarjetas(Long id);

	  /*List<Producto> findAllCompras(Long id);

	  List<Producto> findAllVentas(Long id);

	  List<Producto> findAllMeGusta(Long id);

	  List<Mensaje> findAllMensaje(Long id);

	  List<Mensaje> findConversacion(Long idEnvia, Long idRecibe);

	  List<Mensaje> findAllMensajeById(Long id);*/

	  void deleteById(Long id);

	  void create(Usuario user);

	  void update(Usuario user);

	  void delete (Usuario user);

	}
