package es.udc.tfg.restservice.model.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;


@Entity
public class Pista {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pista_generator")
	@SequenceGenerator(name = "pista_generator", sequenceName = "pista_seq")
	@Column(name = "id_pista")
	private Long Id;

	@ManyToOne
	@JoinColumn(name = "club_pista", nullable = false)
	private Club club;

	@Column(nullable = false)
	private String nombre;

	@Column(nullable = false)
	private TipoPista tipoPista;

	@Column(nullable = false)
	private Boolean estado = false;

	@Column(nullable = false)
	private double precio;
	
	@OneToMany(mappedBy = "pista")
	private List<Partido> partidos = new ArrayList<Partido>();
	
	public Pista() {}

	public Pista(Club club, String nombre, TipoPista tipoPista, Boolean estado, double precio) {
		this.club = club;
		club.addPista(this);
		this.nombre = nombre;
		this.tipoPista = tipoPista;
		this.estado = estado;
		this.precio = precio;
	}

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public TipoPista getTipoPista() {
		return tipoPista;
	}

	public void setTipo(TipoPista tipo) {
		this.tipoPista = tipo;
	}

	public Boolean getEstado() {
		return estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public Club getClub() {
		return club;
	}

	public void setClub(Club club) {
		this.club = club;
	}
	

	/*public List<Partido> getPartidos() {
		return partidos;
	}

	public void setPartidos(List<Partido> partidos) {
		this.partidos = partidos;
	}*/
	
	

}