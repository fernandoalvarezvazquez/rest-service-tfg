package es.udc.tfg.restservice.web;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;

import es.udc.tfg.restservice.model.domain.Club;
import es.udc.tfg.restservice.model.domain.Usuario;
import es.udc.tfg.restservice.model.exception.NotFoundException;
import es.udc.tfg.restservice.model.exception.OperationNotAllowed;
import es.udc.tfg.restservice.model.service.UsuarioService;
import es.udc.tfg.restservice.model.service.dto.ClubDTO;
import es.udc.tfg.restservice.model.service.dto.PartidoDTO;
import es.udc.tfg.restservice.model.service.dto.TarjetaDTO;
import es.udc.tfg.restservice.model.service.dto.UserDTOPrivate;
import es.udc.tfg.restservice.model.service.dto.UserDTOPublic;
import es.udc.tfg.restservice.web.exceptions.IdAndBodyNotMatchingOnUpdateException;
import es.udc.tfg.restservice.web.exceptions.RequestBodyNotValidException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/usuarios")
public class UsuarioResource {

  @Autowired
  private UsuarioService userService;

  @GetMapping
  public List<UserDTOPublic> findAll() {
    return userService.findAll();
  }
  
  @GetMapping("/jugadores")
  public List<UserDTOPublic> findAllJugadores(){
	  return userService.findAllJugadores();
  }

  @GetMapping("/{id}")
  public UserDTOPublic findOne(@PathVariable Long id) throws NotFoundException {
    return userService.findById(id);
  }

  @GetMapping("/perfil")
  public UserDTOPublic findByLogin(@RequestParam String login) throws NotFoundException {
    return userService.findByLogin(login);
  }

  @GetMapping("/{id}/partidos")
  public List<PartidoDTO> findPartidos(@PathVariable Long id) throws NotFoundException{
	return userService.findPartidos(id);
	  
  }
  
  @GetMapping("/{id}/tarjetas")
  public List<TarjetaDTO> findTarjetas(@PathVariable Long id) throws NotFoundException{
	  return userService.findTarjetas(id);
  }
  
  @GetMapping("/{userId}/foto-perfil")
  public ResponseEntity<UrlResource> getProfileImage(@PathVariable Long userId) throws NotFoundException {
      UserDTOPublic user = userService.findById(userId); 
      
      if (user == null || user.getFotoPerfil() == null) {
          return ResponseEntity.notFound().build();
      }
      
      Path path = Paths.get(user.getFotoPerfil());
      UrlResource resource = null;
      
      try {
          resource = new UrlResource(path.toUri());
          if (!resource.exists() || !resource.isReadable()) {
              return ResponseEntity.notFound().build();
          }
      } catch (MalformedURLException e) {
          return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
      }
      
      String contentType = null;
      try {
          contentType = Files.probeContentType(path);
      } catch (IOException e) {
          contentType = "image/jpeg";
      }

      return ResponseEntity.ok()
          .contentType(MediaType.parseMediaType(contentType))
          .body(resource);
  }

	
  @PostMapping("/{userId}/imagen")
  public ResponseEntity<?> uploadImage(@PathVariable Long userId, @RequestParam("file") MultipartFile file) {
      try {
          
          String imageUrl = userService.saveImage(userId, file);
          
          return ResponseEntity.ok(imageUrl);
      } catch (Exception e) {
          // Manejo de errores
          return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error al cargar la imagen: " + e.getMessage());
      }
  }
	
  @PutMapping("/cargar-imagen")
  public ResponseEntity<?> cargarImagen(@ModelAttribute UserDTOPublic user, @RequestParam("imagenArchivo") MultipartFile imagenArchivo) throws IOException{
		
		if (user.getId() == null) {
	        throw new IllegalArgumentException("El ID del usuario es necesario para actualizar");
	    }
		
		if (!imagenArchivo.isEmpty()) {
			try {
				String path = saveFile(imagenArchivo);
				user.setFotoPerfil(path);
			}
			catch (IOException e) {
				e.printStackTrace();
			}
	  }
	
	  userService.updateFoto(user);
	
	  return ResponseEntity.ok().body("Imagen cargada con éxito.");
	}
	
	private String saveFile(MultipartFile file) throws IOException {
	    String fileName = StringUtils.cleanPath(file.getOriginalFilename());
	    File dir = new File("uploads");

	    if (!dir.exists()) {
	    	Boolean success = dir.mkdir();
	    	if(!success)
	    		throw new IOException("no se pudo crear el dir");
	    }

	    File destinationFile = new File(dir, fileName);
	    try (InputStream fileInputStream = file.getInputStream()) {
	        Files.copy(fileInputStream, destinationFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
	    }

	    return destinationFile.getAbsolutePath();
	}
	
  
  

  @PutMapping("/{id}")
  public UserDTOPublic update(@PathVariable Long id, @RequestBody @Valid UserDTOPublic user, Errors errors) 
	  throws IdAndBodyNotMatchingOnUpdateException, RequestBodyNotValidException, NotFoundException{
				
	  if(errors.hasErrors()) {
		  throw new RequestBodyNotValidException(errors);
	  }
	  if (id != user.getId()) {
		  throw new IdAndBodyNotMatchingOnUpdateException(Club.class);
	  }
	  return userService.update(user);
  }
 
  
  @PutMapping("/{id}/nivel")
  public UserDTOPublic updateNivel(@PathVariable Long id, @RequestBody @Valid UserDTOPublic user, Errors errors) 
	  throws IdAndBodyNotMatchingOnUpdateException, RequestBodyNotValidException, NotFoundException{
				
	  if(errors.hasErrors()) {
		  throw new RequestBodyNotValidException(errors);
	  }
	  if (id != user.getId()) {
		  throw new IdAndBodyNotMatchingOnUpdateException(Club.class);
	  }
	  
	  return userService.updateNivel(user);
		
  }
  
  @DeleteMapping("/{id}")
  public void delete(@PathVariable Long id) throws NotFoundException, OperationNotAllowed {

    /*List<ProductoDTO> prodsVenta = userService.findAllVentas(id);
    for(int i = 0; i<prodsVenta.size(); i++)
      if (prodsVenta.get(i).getComprador() == null){
        prodService.deleteById(prodsVenta.get(i).getId());
      }
      else
        prodService.updateVendedor(prodsVenta.get(i));

    List<ProductoDTO> prodsComp = userService.findAllCompras(id);
    for(int i = 0; i<prodsComp.size(); i++)
      prodService.updateComprador(prodsComp.get(i));

    List<ProductoDTO> prodsMeGusta = userService.findAllMeGusta(id);
    for (int i = 0; i<prodsMeGusta.size(); i++)
      prodService.updateMeGusta(prodsMeGusta.get(i), userService.findById(id));

    mensajeService.deleteById(id);*/

    userService.deleteById(id);
  }
}
