package es.udc.tfg.restservice.web;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import es.udc.tfg.restservice.model.domain.Tarjeta;
import es.udc.tfg.restservice.model.exception.NotFoundException;
import es.udc.tfg.restservice.model.exception.OperationNotAllowed;
import es.udc.tfg.restservice.model.service.TarjetaService;
import es.udc.tfg.restservice.model.service.dto.TarjetaDTO;
import es.udc.tfg.restservice.web.exceptions.IdAndBodyNotMatchingOnUpdateException;
import es.udc.tfg.restservice.web.exceptions.RequestBodyNotValidException;

@RestController
@RequestMapping("/api/tarjetas")
public class TarjetaResource {

	@Autowired
	private TarjetaService tarjetaService;
	
	@GetMapping
	public List<TarjetaDTO> findAll(){
		return tarjetaService.findAll();
	}
	
	@GetMapping("/{id}")
	public TarjetaDTO findById(@PathVariable Long id) throws NotFoundException{
		return tarjetaService.findById(id);
	}
	
	@GetMapping("/usuarios/{id}")
	public List<TarjetaDTO> findByPlayer(@PathVariable Long id) throws NotFoundException{
		return tarjetaService.findByPlayer(id);
	}
	
	@PostMapping
	public TarjetaDTO create(@RequestBody @Valid TarjetaDTO tarjeta, Errors errors) throws RequestBodyNotValidException{
		if (errors.hasErrors()) {
			throw new RequestBodyNotValidException(errors);
		}
		return tarjetaService.create(tarjeta);
	}
	
	@PutMapping("/{id}")
	public TarjetaDTO update(@PathVariable Long id, @RequestBody @Valid TarjetaDTO tarjeta, Errors errors)
			throws IdAndBodyNotMatchingOnUpdateException, RequestBodyNotValidException, NotFoundException{
		if (errors.hasErrors()){
			throw new RequestBodyNotValidException(errors);
		}
		if (id != tarjeta.getId()) {
			throw new IdAndBodyNotMatchingOnUpdateException(Tarjeta.class);
		}
		return tarjetaService.update(tarjeta);
	}
	
	@DeleteMapping("/{id}")
	public void delete(@PathVariable Long id) throws NotFoundException, OperationNotAllowed{
		tarjetaService.deleteById(id);
	}
}
