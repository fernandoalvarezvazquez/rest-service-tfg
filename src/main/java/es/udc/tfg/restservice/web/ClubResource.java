package es.udc.tfg.restservice.web;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import es.udc.tfg.restservice.model.domain.Club;
import es.udc.tfg.restservice.model.exception.NotFoundException;
import es.udc.tfg.restservice.model.exception.OperationNotAllowed;
import es.udc.tfg.restservice.model.service.ClubService;
import es.udc.tfg.restservice.model.service.dto.ClubDTO;
import es.udc.tfg.restservice.web.exceptions.IdAndBodyNotMatchingOnUpdateException;
import es.udc.tfg.restservice.web.exceptions.RequestBodyNotValidException;
import io.github.classgraph.Resource;

@RestController
@RequestMapping("/api/clubes")
public class ClubResource {
	
	@Autowired
	private ClubService clubService;
	
	@GetMapping
	public List<ClubDTO> findAll(){
		return clubService.findAll();
	}
	
	@GetMapping("/{id}")
	public ClubDTO findById (@PathVariable Long id) throws NotFoundException{
		return clubService.findById(id);
	}
	
	@GetMapping("/nombre")
	public List<ClubDTO> findByName(@RequestParam String nombre) throws NotFoundException{
		return clubService.findByName(nombre);
	}
	
	@GetMapping("/admin")
	public ClubDTO findByAdmin(@RequestParam Long idAdmin) throws NotFoundException{
		return clubService.findByAdmin(idAdmin);
	}
	
	@GetMapping("/form-carga-imagen")
	public String mostrarImagenForm(Model model) {
		model.addAttribute("club", new Club());
		return "form-carga-imagen";
	}
	
	@GetMapping("/{clubId}/imagen")
    public ResponseEntity<UrlResource> getClubImage(@PathVariable Long clubId) throws NotFoundException {
        ClubDTO club = clubService.findById(clubId);
        
        if (club == null || club.getImagenPath() == null) {
            return ResponseEntity.notFound().build();
        }
        
        Path path = Paths.get(club.getImagenPath());
        UrlResource resource = null;
        
        try {
            resource = new UrlResource(path.toUri());
            if (!resource.exists() || !resource.isReadable()) {
                return ResponseEntity.notFound().build();
            }
        } catch (MalformedURLException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
        
        String contentType = null;
        try {
            contentType = Files.probeContentType(path);
        } catch (IOException e) {
            contentType = "image/jpeg";
        }

        return ResponseEntity.ok()
            .contentType(MediaType.parseMediaType(contentType))
            .body(resource);
    }

	
	@PostMapping("/{clubId}/imagen")
    public ResponseEntity<?> uploadImage(@PathVariable Long clubId, @RequestParam("file") MultipartFile file) {
        try {
            
            String imageUrl = clubService.saveImage(clubId, file);
            
            return ResponseEntity.ok(imageUrl);
        } catch (Exception e) {
            // Manejo de errores
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error al cargar la imagen: " + e.getMessage());
        }
    }
	
	@PutMapping("/cargar-imagen")
	public ResponseEntity<?> cargarImagen(@ModelAttribute ClubDTO club, @RequestParam("imagenArchivo") MultipartFile imagenArchivo) throws IOException{
		
		if (club.getId() == null) {
	        throw new IllegalArgumentException("El ID del club es necesario para actualizar");
	    }
		
		if (!imagenArchivo.isEmpty()) {
			try {
				String path = saveFile(imagenArchivo);
				club.setImagenPath(path);
			}
			catch (IOException e) {
				e.printStackTrace();
			}
        }

        clubService.update(club);

        return ResponseEntity.ok().body("Imagen cargada con éxito.");
	}
	
	private String saveFile(MultipartFile file) throws IOException {
	    String fileName = StringUtils.cleanPath(file.getOriginalFilename());
	    File dir = new File("uploads");

	    if (!dir.exists()) {
	    	Boolean success = dir.mkdir();
	    	if(!success)
	    		throw new IOException("no se pudo crear el dir");
	    }

	    File destinationFile = new File(dir, fileName);
	    try (InputStream fileInputStream = file.getInputStream()) {
	        Files.copy(fileInputStream, destinationFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
	    }

	    return destinationFile.getAbsolutePath();
	}
	
	@PostMapping
	public ClubDTO create(@RequestBody @Valid ClubDTO c, Errors errors) throws RequestBodyNotValidException{
		if (errors.hasErrors()) {
			throw new RequestBodyNotValidException(errors);
		}
		return clubService.create(c);
	}
	
	@PutMapping("/{id}")
	public ClubDTO update(@PathVariable Long id, @RequestBody @Valid ClubDTO club, Errors errors) 
		throws IdAndBodyNotMatchingOnUpdateException, RequestBodyNotValidException, NotFoundException{
		if(errors.hasErrors()) {
			throw new RequestBodyNotValidException(errors);
		}
		if (id != club.getId()) {
			throw new IdAndBodyNotMatchingOnUpdateException(Club.class);
		}
		return clubService.update(club);
	}
	
	@DeleteMapping("/{id}")
	public void delete(@PathVariable Long id) throws NotFoundException, OperationNotAllowed{
		clubService.deleteById(id);
	}
}





