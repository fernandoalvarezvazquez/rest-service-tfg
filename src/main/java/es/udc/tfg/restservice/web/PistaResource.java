package es.udc.tfg.restservice.web;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import es.udc.tfg.restservice.model.domain.Club;
import es.udc.tfg.restservice.model.domain.Pista;
import es.udc.tfg.restservice.model.exception.NotFoundException;
import es.udc.tfg.restservice.model.exception.OperationNotAllowed;
import es.udc.tfg.restservice.model.service.PistaService;
import es.udc.tfg.restservice.model.service.dto.PistaDTO;
import es.udc.tfg.restservice.web.exceptions.IdAndBodyNotMatchingOnUpdateException;
import es.udc.tfg.restservice.web.exceptions.RequestBodyNotValidException;

@RestController
@RequestMapping("/api/pistas")
public class PistaResource {

	@Autowired
	private PistaService pistaService;
	
	@GetMapping
	public List<PistaDTO> findAllByClub(@RequestParam Long id) throws NotFoundException{
		return pistaService.findAllByClub(id);
	}
	
	@GetMapping("/{id}")
	public PistaDTO findById(@PathVariable Long id) throws NotFoundException{
		return pistaService.findById(id);
	}
	
	@PostMapping
	public PistaDTO create(@RequestBody @Valid PistaDTO p, Errors errors) throws RequestBodyNotValidException {
		if (errors.hasErrors()) {
			throw new RequestBodyNotValidException(errors);
		}
		return pistaService.create(p);
	}
	
	@PutMapping("/{id}")
	public PistaDTO update(@PathVariable Long id, @RequestBody PistaDTO pista, Errors errors)
			throws IdAndBodyNotMatchingOnUpdateException, RequestBodyNotValidException, NotFoundException{
		if(errors.hasErrors()) {
			throw new RequestBodyNotValidException(errors);
		}
		
		if (id != pista.getId()) {
			throw new IdAndBodyNotMatchingOnUpdateException(Pista.class);
		}
		return pistaService.update(pista);
	}
	
	@DeleteMapping("/{id}")
	public void delete(@PathVariable Long id) throws NotFoundException, OperationNotAllowed{
		pistaService.deleteById(id);
	}
	
}
