package es.udc.tfg.restservice.web;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.paypal.api.payments.Amount;
import com.paypal.api.payments.Links;
import com.paypal.api.payments.Payer;
import com.paypal.api.payments.Payment;
import com.paypal.api.payments.RedirectUrls;
import com.paypal.api.payments.Transaction;
import com.paypal.base.rest.APIContext;
import com.paypal.base.rest.PayPalRESTException;

import es.udc.tfg.restservice.model.domain.Tarjeta;
import es.udc.tfg.restservice.model.domain.Transaccion;
import es.udc.tfg.restservice.model.exception.NotFoundException;
import es.udc.tfg.restservice.model.service.TransaccionService;
import es.udc.tfg.restservice.model.service.dto.TransaccionDTO;
import es.udc.tfg.restservice.web.exceptions.IdAndBodyNotMatchingOnUpdateException;
import es.udc.tfg.restservice.web.exceptions.RequestBodyNotValidException;

@RestController
@RequestMapping("/api/transacciones")
public class TransaccionResource {

	@Autowired
	private TransaccionService transaccionService;
	
	@Autowired
    private APIContext apiContext;
	
	@GetMapping
	public List<TransaccionDTO> findAll(){
		return transaccionService.findAll();
	}
	
	@GetMapping("/{id}")
	public TransaccionDTO findById(@PathVariable Long id) throws NotFoundException{
		return transaccionService.findById(id);
	}
	
	@PostMapping
	public TransaccionDTO create(@RequestBody @Valid TransaccionDTO transaccion, Errors errors)
		throws RequestBodyNotValidException{
		if (errors.hasErrors()){
			throw new RequestBodyNotValidException(errors);
		}
		
		return transaccionService.create(transaccion);
	}
	
	@PostMapping("/iniciarPago")
    public ResponseEntity<String> iniciarPago(@RequestParam double precio) {

        Amount amount = new Amount();
        amount.setCurrency("EUR"); 
        amount.setTotal(String.valueOf(precio)); 
        
        Transaction transaccion = new Transaction();
        transaccion.setAmount(amount);
        List<Transaction> transacciones = new ArrayList<>();
        transacciones.add(transaccion);

        Payer payer = new Payer();
        payer.setPaymentMethod("paypal");

        Payment payment = new Payment();
        payment.setIntent("sale");
        payment.setPayer(payer);
        payment.setTransactions(transacciones);
        
        // URLs de redirección después del pago
        RedirectUrls redirectUrls = new RedirectUrls();
        redirectUrls.setCancelUrl("http://tuapp.com/cancelled");
        redirectUrls.setReturnUrl("http://tuapp.com/confirmed");
        //redirectUrls.setReturnUrl("https://www.google.com");
        payment.setRedirectUrls(redirectUrls);

        try {
            Payment createdPayment = payment.create(apiContext);
            for(Links link : createdPayment.getLinks()) {
                if(link.getRel().equalsIgnoreCase("approval_url")) {
                    return ResponseEntity.ok(link.getHref());
                }
            }
        } catch (PayPalRESTException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error al iniciar el pago.");
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error desconocido al iniciar el pago.");
    }
	
	@PutMapping("/{id}/cambiar-estado")
	public TransaccionDTO cambiarEstado(@PathVariable Long id, @RequestBody @Valid TransaccionDTO tran, Errors errors)
		throws IdAndBodyNotMatchingOnUpdateException, RequestBodyNotValidException, NotFoundException {
		if (errors.hasErrors()){
			throw new RequestBodyNotValidException(errors);
		}
		if (id != tran.getId()) {
			throw new IdAndBodyNotMatchingOnUpdateException(Transaccion.class);
		}
		return transaccionService.cambiarEstado(tran);
	}
	
}
