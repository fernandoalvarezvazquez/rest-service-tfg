package es.udc.tfg.restservice.web;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import es.udc.tfg.restservice.model.domain.BonoClases;
import es.udc.tfg.restservice.model.exception.NotFoundException;
import es.udc.tfg.restservice.model.exception.OperationNotAllowed;
import es.udc.tfg.restservice.model.service.BonoClasesService;
import es.udc.tfg.restservice.model.service.dto.BonoClasesDTO;
import es.udc.tfg.restservice.model.service.dto.UserDTOPublic;
import es.udc.tfg.restservice.web.exceptions.RequestBodyNotValidException;

@RestController
@RequestMapping("/api/bonos")
public class BonoClasesResource {
	
	@Autowired
	private BonoClasesService bonoService;
	
	@GetMapping
	public List<BonoClasesDTO> findAll() {
		return bonoService.findAll();
	}
	
	@GetMapping("/club/{idClub}")
	public List<BonoClasesDTO> findAllByClub(@PathVariable Long idClub) throws NotFoundException {
		return bonoService.findAllByClub(idClub);
	}
	
	@GetMapping("/{id}")
	public BonoClasesDTO findById(@PathVariable Long id) throws NotFoundException{
		return bonoService.findById(id);
	}
	
	@GetMapping("/jugador/{id}")
	public List<BonoClasesDTO> findByJugador(@PathVariable Long idUser) throws NotFoundException{
		return bonoService.findByJugador(idUser);
	}
	
	@PostMapping
	public BonoClasesDTO create (@RequestBody @Valid BonoClasesDTO bono, Errors errors) throws RequestBodyNotValidException{
		if (errors.hasErrors()) {
			throw new RequestBodyNotValidException(errors);
		}
		return bonoService.create(bono);
	}
	
	@PutMapping("/{id}")
	public BonoClasesDTO update(@PathVariable Long id, @RequestBody @Valid BonoClasesDTO bono, Errors errors) throws NotFoundException, RequestBodyNotValidException{
		if (errors.hasErrors()) {
			throw new RequestBodyNotValidException(errors);
		}
		
		if (id != bono.getId()) {
			throw new NotFoundException(id.toString(), BonoClases.class);
		}
		return bonoService.update(bono);
	}
	
	@PutMapping("/{id}/comprador")
	public BonoClasesDTO updateComprador(@PathVariable Long id, @RequestBody @Valid BonoClasesDTO bono, Errors errors) throws NotFoundException, RequestBodyNotValidException{
		if (errors.hasErrors()) {
			throw new RequestBodyNotValidException(errors);
		}
		
		if (id != bono.getId()) {
			throw new NotFoundException(id.toString(), BonoClases.class);
		}
		return bonoService.updateComprador(bono);
	}
	
	@DeleteMapping("/{id}")
	public void delete(@PathVariable Long id) throws NotFoundException, OperationNotAllowed{
		bonoService.deleteById(id);
	}
}








