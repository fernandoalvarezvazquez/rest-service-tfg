package es.udc.tfg.restservice.web;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import es.udc.tfg.restservice.model.exception.NotFoundException;
import es.udc.tfg.restservice.model.exception.UserLoginExistsException;
import es.udc.tfg.restservice.model.service.UsuarioService;
import es.udc.tfg.restservice.model.service.dto.LoginDTO;
import es.udc.tfg.restservice.model.service.dto.UserDTOPrivate;
import es.udc.tfg.restservice.security.JWTToken;
import es.udc.tfg.restservice.security.TokenProvider;
import es.udc.tfg.restservice.web.exceptions.CredentialsAreNotValidException;
import es.udc.tfg.restservice.web.exceptions.IdAndBodyNotMatchingOnUpdateException;
import es.udc.tfg.restservice.web.exceptions.RequestBodyNotValidException;

/**
 * Este controlador va por separado que el UserResource porque se encarga de
 * tareas relacionadas con la autenticación, registro, etc.
 *
 * <p>
 * También permite a cada usuario logueado en la aplicación obtener información
 * de su cuenta
 */
@RestController
@RequestMapping("/api")
public class AccountResource {
  private final Logger logger = LoggerFactory.getLogger(AccountResource.class);

  @Autowired
  private TokenProvider tokenProvider;

  @Autowired
  private AuthenticationManager authenticationManager;

  @Autowired
  private UsuarioService userService;

  @PostMapping("/authenticate")
  public JWTToken authenticate(@Valid @RequestBody LoginDTO loginDTO) throws CredentialsAreNotValidException {

    UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
        loginDTO.getLogin(), loginDTO.getPassword());
    try {
      Authentication authentication = authenticationManager.authenticate(authenticationToken);
      SecurityContextHolder.getContext().setAuthentication(authentication);
      String jwt = tokenProvider.createToken(authentication);
      return new JWTToken(jwt);
    } catch (AuthenticationException e) {
      logger.warn(e.getMessage(), e);
      throw new CredentialsAreNotValidException(e.getMessage());
    }
  }

  @GetMapping("/account")
  public UserDTOPrivate getAccount() {
    return userService.getCurrentUserWithAuthority();
  }

  @PostMapping("/register")
  public void registerAccount(@Valid @RequestBody UserDTOPrivate account, Errors errors)
      throws UserLoginExistsException, RequestBodyNotValidException {
    if (errors.hasErrors()) {
      throw new RequestBodyNotValidException(errors);
    }

    userService.registerUser(account.getLogin(), account.getContrasena());
  }
  
  @PostMapping("/register-admin-club")
  public void registerAccountClub(@Valid @RequestBody UserDTOPrivate account, Errors errors)
      throws UserLoginExistsException, RequestBodyNotValidException {
    if (errors.hasErrors()) {
      throw new RequestBodyNotValidException(errors);
    }

    userService.registerUser(account.getLogin(), account.getContrasena(), true, false);
  }
  
  @PostMapping("/register-admin-web")
  public void registerAccountWeb(@Valid @RequestBody UserDTOPrivate account, Errors errors)
      throws UserLoginExistsException, RequestBodyNotValidException {
    if (errors.hasErrors()) {
      throw new RequestBodyNotValidException(errors);
    }

    userService.registerUser(account.getLogin(), account.getContrasena(), true, true);
  }

  @PutMapping("/password/{id}")
  public UserDTOPrivate changePassword(@PathVariable Long id, @RequestBody @Valid LoginDTO login, Errors errors)
    throws IdAndBodyNotMatchingOnUpdateException, RequestBodyNotValidException, NotFoundException, CredentialsAreNotValidException {
    if (errors.hasErrors()) {
      throw new RequestBodyNotValidException(errors);
    }

    //if (id != userService.findByLogin(login.getLogin()).getId()) {
    //  throw new IdAndBodyNotMatchingOnUpdateException(Producto.class);
    //}
    UserDTOPrivate user = userService.updateContrasena(login.getLogin(), login.getPassword());
    //authenticate(login);
    return user;
  }

}
