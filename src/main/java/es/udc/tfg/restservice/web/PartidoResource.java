package es.udc.tfg.restservice.web;

import java.time.LocalDateTime;
import java.util.List;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import es.udc.tfg.restservice.model.domain.Partido;
import es.udc.tfg.restservice.model.domain.Usuario;
import es.udc.tfg.restservice.model.exception.NotFoundException;
import es.udc.tfg.restservice.model.exception.OperationNotAllowed;
import es.udc.tfg.restservice.model.repository.UsuarioDao;
import es.udc.tfg.restservice.model.service.PartidoService;
import es.udc.tfg.restservice.model.service.dto.PartidoDTO;
import es.udc.tfg.restservice.web.exceptions.IdAndBodyNotMatchingOnUpdateException;
import es.udc.tfg.restservice.web.exceptions.RequestBodyNotValidException;

@RestController
@RequestMapping("/api/partidos")
public class PartidoResource {

	@Autowired
	private PartidoService partidoService;
	
	@Autowired
	private UsuarioDao usuarioDao;
	
	@GetMapping
	public List<PartidoDTO> findAll(){
		return partidoService.findAll();
	}
	
	@GetMapping("/{id}")
	public PartidoDTO findById(@PathVariable Long id) throws NotFoundException{
		return partidoService.findById(id);
	}
	
	@GetMapping("/find")
	public PartidoDTO findByPistaHora(@RequestParam Long idPista, @RequestParam String hora) throws NotFoundException {
		LocalDateTime hora_ini = LocalDateTime.parse(hora);
		return partidoService.findByPistaHora(idPista, hora_ini);
	}
	
	@GetMapping("/club/{id}")
	public List<PartidoDTO> findByClub(@PathVariable Long id) throws NotFoundException{
		return partidoService.findByClub(id);
		
	}
	
	@PostMapping
	public PartidoDTO create(@RequestBody @Valid PartidoDTO p, Errors errors) throws RequestBodyNotValidException {
		
		if (errors.hasErrors()) {
			throw new RequestBodyNotValidException(errors);
		}
		
		try {
	        return partidoService.create(p);
	    } catch (ConstraintViolationException ex) {
	        ResponseEntity.badRequest().body("Esta pista y horario ya están ocupados.");
	    }
		
		return partidoService.create(p);
	}
	
	@PutMapping("/{id}")
	public PartidoDTO update(@PathVariable Long id, @RequestBody PartidoDTO partido, Errors errors)
	throws IdAndBodyNotMatchingOnUpdateException, RequestBodyNotValidException, NotFoundException{
		if(errors.hasErrors()) {
			throw new RequestBodyNotValidException(errors);
		}
		if (id != partido.getId()) {
			throw new IdAndBodyNotMatchingOnUpdateException(Partido.class);
		}
		return partidoService.update(partido);
	}
	
	@PutMapping("/{id}/add-jugador")
	public PartidoDTO addJugador(@PathVariable Long id, @RequestBody PartidoDTO partido, @RequestParam Long idJugador, Errors errors) 
			throws RequestBodyNotValidException, IdAndBodyNotMatchingOnUpdateException {
		if(errors.hasErrors()) {
			throw new RequestBodyNotValidException(errors);
		}
		if (id != partido.getId()) {
			throw new IdAndBodyNotMatchingOnUpdateException(Partido.class);
		}
		
		Usuario jugador = usuarioDao.findById(idJugador);
		
		return partidoService.addJugador(partido, jugador);
	}
	
	@PutMapping("/{id}/delete-jugador")
	public PartidoDTO deleteJugador(@PathVariable Long id, @RequestBody PartidoDTO partido, @RequestParam Long idJugador, Errors errors) 
			throws RequestBodyNotValidException, IdAndBodyNotMatchingOnUpdateException{
		if(errors.hasErrors()) {
			throw new RequestBodyNotValidException(errors);
		}
		if (id != partido.getId()) {
			throw new IdAndBodyNotMatchingOnUpdateException(Partido.class);
		}
		
		Usuario jugador = usuarioDao.findById(idJugador);	
		
		return partidoService.deleteJugador(partido, jugador);
	}
	
	@PutMapping("/{id}/resultado")
	public PartidoDTO updateResultado(@PathVariable Long id, @RequestBody @Valid PartidoDTO partido, Errors errors)
	throws IdAndBodyNotMatchingOnUpdateException, RequestBodyNotValidException, NotFoundException {
		if (errors.hasErrors()) {
	      throw new RequestBodyNotValidException(errors);
	    }

		if (id != partido.getId())
			throw new IdAndBodyNotMatchingOnUpdateException(Partido.class);
		
		return partidoService.updateResultado(partido);
	}
	
	@PutMapping("/{id}/pagado")
	public PartidoDTO updatePagado(@PathVariable Long id, @RequestBody @Valid PartidoDTO partido, Errors errors) 
			throws IdAndBodyNotMatchingOnUpdateException, RequestBodyNotValidException, NotFoundException{
		if (errors.hasErrors()) {
			throw new RequestBodyNotValidException(errors);
	    }
		if (id != partido.getId())
			throw new IdAndBodyNotMatchingOnUpdateException(Partido.class);
		
		return partidoService.updatePagado(partido);
		
	}
	
	@DeleteMapping("/{id}")
	public void delete(@PathVariable Long id) throws NotFoundException, OperationNotAllowed {
		partidoService.deleteById(id);
	}
}
